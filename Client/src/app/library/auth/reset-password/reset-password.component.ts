import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../../services/userService/users.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  constructor(
    private User: UsersService,
    private route: ActivatedRoute,
    private root: Router,
    private messageService: ToastrService)  { }
  ngOnInit() {
    this.resetPasswordForm = new FormGroup({
      "psw": new FormControl('', [Validators.required, Validators.maxLength(5)]),
      "psw2": new FormControl('', [Validators.required, Validators.maxLength(5)]),
    })
  }
  onSubmit() {
    console.log(this.resetPasswordForm.value);
    if (this.resetPasswordForm.value.psw == this.resetPasswordForm.value.psw2) {
      this.route.params.subscribe(params => {
        this.User.resetPassword(this.resetPasswordForm.value, params['token'])
          .subscribe((res: any) => {
            if (res.code == 200) {
              this.messageService.success(res.message);
              res = res.data;
            } else {
              this.messageService.error(res.message);
            }
            res = res.data;
          });
      });
      this.root.navigate(["/login"]);
      this.resetPasswordForm.reset();
    }
    else {
      this.resetPasswordForm.reset();
      alert("Both fields must be same and less than 5 letters.")

    }
  }
}
