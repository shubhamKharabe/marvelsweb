import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../../services/userService/users.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  constructor(
    private User: UsersService,
    private route: Router,
    private messageService: ToastrService) { }
  ngOnInit() {
    this.forgotPasswordForm = new FormGroup({
      "email": new FormControl('', Validators.compose([Validators.required, Validators.email])),
    })
  }
  onSubmit() {
    console.log(this.forgotPasswordForm.value);
    this.User.forgotPassword(this.forgotPasswordForm.value)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else {
          this.messageService.error(res.message);

        }
      });
    // this.route.navigate(["/login"]);
    this.forgotPasswordForm.reset();
  }
}
