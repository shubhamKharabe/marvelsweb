import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../../../services/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  logInForm: FormGroup

  constructor(private login: LoginService,
    private router: Router,
    private messageService: ToastrService) { }

  ngOnInit() {
    this.logInForm = new FormGroup({
      "uname": new FormControl('', [Validators.required, Validators.maxLength(15)]),
      "psw": new FormControl('', [Validators.required, Validators.maxLength(5)]),
    });
  }
  onSubmit() {
    console.log("jhgj", this.logInForm.value);
    this.login.login(this.logInForm.value)
      .subscribe((res: any) => {
        console.log("saghdshja", res);
        if (res.code == 404) {
          this.messageService.error(res.message);
        } else {
          this.messageService.success(res.message);
          var token = res.token;
          var role = res.user.role;
          var uname = res.user.uname;
          var id = res.user._id;
          localStorage.setItem('token', token);
          localStorage.setItem('role', role);
          localStorage.setItem('uname', uname);
          localStorage.setItem('id', id);
          if (localStorage.role == "admin") {
            this.router.navigate(['adminDashboard']);
          }
          else {
            this.router.navigate(['dashboard']);
          }
        }
      });
    this.logInForm.reset();
  };

}
