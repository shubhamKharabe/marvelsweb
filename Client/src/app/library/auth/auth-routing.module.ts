import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

const route:Routes=[
  {path:"", component:LoginComponent},
  {path:"signUp", component:SignUpComponent},
  {path:"forgotPassword", component:ForgotPasswordComponent},
  {path:"resetPassword/:token", component:ResetPasswordComponent},
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AuthRoutingModule { }
