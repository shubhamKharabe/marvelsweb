import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SignUpService } from '../../../services/sign-up.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  signUpForm: FormGroup;
  //PrimeNG
  gender: SelectItem[];
  value: Date;
  display: boolean = false;
  pictures = [
    { pic: '../../assets/images/1.jpg' },
    { pic: '../../assets/images/2.jpg' },
    { pic: '../../assets/images/3.jpg' },
    { pic: '../../assets/images/4.jpg' }
  ];
  showDialog() {
    this.display = true;
  }
  //PrimeNG Ends

  constructor(private signUp: SignUpService,
    private route: Router,
    private messageService: ToastrService) {
    this.gender = [
      { label: 'Select Gender', value: null },
      { label: 'Male', value: 'male' },
      { label: 'Female', value: 'female' },
      { label: 'Other', value: 'other' },
    ];
  }
  ngOnInit() {
    this.signUpForm = new FormGroup({
      "uname": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]),
      "psw": new FormControl('', [Validators.required, Validators.maxLength(8)]),
      "email": new FormControl('', Validators.compose([Validators.required, Validators.email])),
      "bdate": new FormControl('', [Validators.required]),
      "gender": new FormControl('', [Validators.required]),
    })
  }
  onSubmit() {
    console.log(this.signUpForm.value);
    if (this.signUpForm.value.bdate > Date.now()) {
      this.messageService.error("Bhoot! Bhoot! Haven't u born yet?");
      return null;
    }
    this.signUp.SignUp(this.signUpForm.value)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
          this.route.navigate(["/login"]);
        } else {
          this.messageService.error("UserName or Email already exists!");
          this.signUpForm.reset();
        }
      });
  }
}
