import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './library/shared/components/home/home.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { LoginService } from './services/login.service';
import { InterceptorsService } from './services/interceptors.service';
import { AuthguardService } from './services/authguard.service';
import { LogedOutService } from './services/loged-out.service';
import { ToastrModule } from 'ngx-toastr';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    TooltipModule.forRoot(),
    ToastrModule.forRoot(),
  ],
  providers: [LoginService,AuthguardService,LogedOutService,
  {provide: HTTP_INTERCEPTORS,useClass:InterceptorsService, multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
