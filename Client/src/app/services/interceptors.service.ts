import { Injectable } from '@angular/core';
import { HttpRequest, HttpInterceptor, HttpHandler } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InterceptorsService implements HttpInterceptor {
  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    var token = localStorage.getItem('token')
    const authRequest = req.clone({
      headers: req.headers.set("authorization", "Bearer " + token)
    });
    // console.log("+++++++++++++++++++", authRequest)
    return next.handle(authRequest);
  }
}
