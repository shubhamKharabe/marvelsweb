import { TestBed, inject } from '@angular/core/testing';

import { AuthguardServiceForUserService } from './authguard-service-for-user.service';

describe('AuthguardServiceForUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthguardServiceForUserService]
    });
  });

  it('should be created', inject([AuthguardServiceForUserService], (service: AuthguardServiceForUserService) => {
    expect(service).toBeTruthy();
  }));
});
