import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartServiceService {
  apiUrl = environment.url;
  constructor(private http: HttpClient) { }

  listData(userId): Observable<any> {
    return this.http.get(this.apiUrl + 'Cart?userId=' + userId);
  }

  addToCart(data: any): Observable<any> {
    console.log("add cart service", data);
    return this.http.post(this.apiUrl + 'Cart', data);
  }

  editCart(data: any): Observable<any> {
    console.log("add cart service", data);
    return this.http.put(this.apiUrl + 'Cart', data);
  }

  deleteData(id): Observable<any> {
    console.log("Deleted", id)
    return this.http.delete(this.apiUrl + 'Cart?id=' + id);
  }

  DeleteAllCart(id): Observable<any> {
    console.log("Deleted", id)
    return this.http.delete(this.apiUrl + 'DeleteAllCart?id=' + id);
  }
}