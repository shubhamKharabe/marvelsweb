import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  apiUrl = environment.url;
  constructor(private http: HttpClient) { }
  addData(data: any): Observable<any> {
    console.log("Added", data);
    return this.http.post(this.apiUrl + 'products', data);
  }
  deleteData(id): Observable<any>{
    console.log("Deleted")
    return this.http.delete(this.apiUrl + 'products?id=' + id);
  }
  editData(data: any, id): Observable<any> {
    console.log("edit", data);
    return this.http.put(this.apiUrl + 'products?id=' + id, data);
  }
  listProductData(id): Observable<any> {
    return this.http.get(this.apiUrl + 'getProduct?id=' + id);
  }
  listData(): Observable<any>{
    return this.http.get(this.apiUrl + 'products');
  }
}
