import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogedOutService {

  constructor(private log: LoginService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.log.isLoged()) {
      if (localStorage.role == "admin") {
        this.router.navigate(["/adminDashboard"]);
      }
      else {
        this.router.navigate(["/dashboard"]);
      }
      return false;
    } else {
      return true;
    }
  }
}
