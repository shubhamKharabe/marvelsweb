import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  apiUrl = environment.url;
  constructor(private http: HttpClient) { }

  addToOrders(data: any): Observable<any> {
    console.log("add data service", data);
    return this.http.post(this.apiUrl + 'Orders/', data);
  }


  listOrders(userId): Observable<any> {
    return this.http.get(this.apiUrl + 'Orders?userId=' + userId);
  }
}
