import { TestBed, inject } from '@angular/core/testing';

import { LogedOutService } from './loged-out.service';

describe('LogedOutService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LogedOutService]
    });
  });

  it('should be created', inject([LogedOutService], (service: LogedOutService) => {
    expect(service).toBeTruthy();
  }));
});
