import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  apiUrl = environment.url;
  constructor(private http: HttpClient) { }
  login(data: any): Observable<any> {
    console.log("API Hitted");
    return this.http.post(this.apiUrl + 'login', data);
  }

  isLoged() {
    if ((localStorage.getItem("token")) !== null)
      return true;
  }

  isLogedAsUser() {
    if ((localStorage.getItem("token")) !== null && (localStorage.getItem("role")) == "user")
      return true;
  }
  isLogedAsAdmin() {
    if ((localStorage.getItem("token")) !== null && (localStorage.getItem("role")) == "admin")
      return true;
  }
}