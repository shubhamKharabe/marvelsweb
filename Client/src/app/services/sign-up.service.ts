import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {
  apiUrl = environment.url;
  constructor(private http: HttpClient) { }
  SignUp(data: any): Observable<any> {
    console.log("API Hitted");
    return this.http.post(this.apiUrl + 'users', data);
  }
}
