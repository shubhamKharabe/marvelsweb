import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthguardServiceForUserService {

  constructor(private log: LoginService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.log.isLogedAsUser()) {
      return true;
    } else if (this.log.isLogedAsAdmin()) {
      this.router.navigate(["/adminDashboard"]);
      return false;
    } else {
      this.router.navigate([""]);
      return false;
    }
  }
}
