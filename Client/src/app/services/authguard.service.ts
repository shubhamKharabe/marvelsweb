import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService {

  constructor(private log: LoginService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.log.isLogedAsAdmin()) {
      return true;
    } else if (this.log.isLogedAsUser()) {
      this.router.navigate(["/dashboard"]);
      return false;
    } else {
      this.router.navigate([""]);
      return false;
    }
  }
}
