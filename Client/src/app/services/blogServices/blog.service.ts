import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  apiUrl = environment.url;
  constructor(private http: HttpClient) { }

  addBlog(data: any): Observable<any> {
    console.log("Added", data);
    return this.http.post(this.apiUrl + 'blogs', data);
  }

  listBlogs(): Observable<any> {
    return this.http.get(this.apiUrl + 'blogs');
  }

  deleteBlog(id): Observable<any> {
    console.log("Deleted")
    return this.http.delete(this.apiUrl + 'blogs?id=' + id);
  }

  editData(data: any, id): Observable<any> {
    console.log("edit", data, id);
    return this.http.put(this.apiUrl + 'blogs?id=' + id, data);
  }

  getBlog(id): Observable<any> {
    return this.http.get(this.apiUrl +'getBlog?id=' + id);
  }

  addComment(data: any): Observable<any> {
    console.log("add comment service", data);
    return this.http.put(this.apiUrl + 'blogComments/', data);
  }

  getComments(id): Observable<any> {
    return this.http.get(this.apiUrl + 'blogComments?id=' + id);
  }
}
