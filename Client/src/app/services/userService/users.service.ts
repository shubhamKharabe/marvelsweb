import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiUrl = environment.url;
  constructor(private http: HttpClient) { }
  add(data: any): Observable<any> {
    console.log("sdf", data);
    return this.http.post(this.apiUrl + 'users', data);
  }
  list(): Observable<any> {
    return this.http.get(this.apiUrl + 'users');
  }
  edit(data: any, id): Observable<any> {
    console.log("edit", data);
    return this.http.put(this.apiUrl + 'users?id=' + id, data);
  }
  delete(id): Observable<any> {
    console.log("Deleted")
    return this.http.delete(this.apiUrl + 'users?id=' + id);
  }
  listUser(id): Observable<any> {
    return this.http.get(this.apiUrl + 'getUser?id=' + id);
  }
  editUser(data: any): Observable<any> {
    console.log("sdf", data);
    return this.http.post(this.apiUrl + 'editProfile/', data);
  }
  forgotPassword(data: any): Observable<any> {
    console.log("sdf", data);
    return this.http.post(this.apiUrl + 'forgotPassword/', data);
  }
  resetPassword(data: any, token: any): Observable<any> {
    console.log("sdf", data);
    return this.http.put(this.apiUrl + 'forgotPassword/?token=' + token, data);
  }
  // addImage(data: any): Observable<any> {
  //   console.log("sdf", data);
  //   return this.http.post(this.apiUrl + 'fileManagement', data);
  // }
}