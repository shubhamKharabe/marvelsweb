import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {
  apiUrl = environment.url;
  // constructor(private http: HttpClient) { }
  // done(data: any): Observable<any> {
  //   console.log("Added", data);
  //   return this.http.post(this.apiUrl + 'Payment', data);
  // }
  constructor(private http: HttpClient) { }
  done(data: any): Observable<any> {
    console.log("Added", data);
    return this.http.post(this.apiUrl + 'Payment', data);
  }

}
