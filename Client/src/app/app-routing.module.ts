import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './library/shared/components/home/home.component';
import { AuthguardService } from './services/authguard.service';
import { LogedOutService } from './services/loged-out.service';
import { AuthguardServiceForUserService } from './services/authguard-service-for-user.service';

const routes: Routes=[
  {path: "", component:HomeComponent },
  {path: "login", loadChildren: "./library/auth/auth.module#AuthModule",canActivate:[LogedOutService]},
  {path: "dashboard", loadChildren: "./areas/users/users.module#UsersModule",canActivate:[AuthguardServiceForUserService]},
  {path: "adminDashboard", loadChildren: "./areas/admin/admin.module#AdminModule",canActivate:[AuthguardService]},
  {path: "servicePages", loadChildren: "./areas/globalPages/service-pages/service-pages.module#ServicePagesModule" },
  {path: "**", loadChildren: "./library/auth/auth.module#AuthModule",canActivate:[LogedOutService]},
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class AppRoutingModule { }
