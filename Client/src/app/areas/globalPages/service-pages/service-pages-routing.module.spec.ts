import { ServicePagesRoutingModule } from './service-pages-routing.module';

describe('ServicePagesRoutingModule', () => {
  let servicePagesRoutingModule: ServicePagesRoutingModule;

  beforeEach(() => {
    servicePagesRoutingModule = new ServicePagesRoutingModule();
  });

  it('should create an instance', () => {
    expect(servicePagesRoutingModule).toBeTruthy();
  });
});
