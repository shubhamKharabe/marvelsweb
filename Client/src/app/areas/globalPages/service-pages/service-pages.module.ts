import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './faq/faq.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ServicePagesRoutingModule } from './/service-pages-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ServicePagesRoutingModule
  ],
  declarations: [FaqComponent, PrivacyPolicyComponent]
})
export class ServicePagesModule { }
