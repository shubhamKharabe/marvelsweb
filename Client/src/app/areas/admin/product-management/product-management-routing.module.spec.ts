import { ProductManagementRoutingModule } from './product-management-routing.module';

describe('ProductManagementRoutingModule', () => {
  let productManagementRoutingModule: ProductManagementRoutingModule;

  beforeEach(() => {
    productManagementRoutingModule = new ProductManagementRoutingModule();
  });

  it('should create an instance', () => {
    expect(productManagementRoutingModule).toBeTruthy();
  });
});
