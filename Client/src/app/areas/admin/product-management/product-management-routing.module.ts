import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductListingComponent } from './product-listing/product-listing.component';
import { EditProductComponent } from './edit-product/edit-product.component';

const routes: Routes=[
  {path: "addp", component: AddProductComponent },
  {path: "listp", component: ProductListingComponent},
  {path: "editp/:id", component: EditProductComponent},
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class ProductManagementRoutingModule { }
