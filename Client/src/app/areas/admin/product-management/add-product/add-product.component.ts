import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductsService } from '../../../../services/productServices/products.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})

export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;
  status: SelectItem[];
  constructor(private Product: ProductsService,
    private formBuild: FormBuilder,
    private root: Router,
    private messageService: ToastrService) {
    this.status = [
      { label: 'Select Status', value: null },
      { label: 'Out-Of-Stock', value: 'out-of-stock' },
      { label: 'In-Stock', value: 'in-stock' },
    ];
  }


  ngOnInit() {
    this.addProductForm = this.formBuild.group({
      pname: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      imagePath: new FormControl('', [Validators.required]),
      pcost: new FormControl('', [Validators.required, Validators.maxLength(5)]),
      description: new FormControl('', [Validators.required, Validators.minLength(50)]),
      status: new FormControl('', [Validators.required]),
    });
  }

  onUpload(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.addProductForm.get('imagePath').setValue(file);
      console.log(file);
    }
  }
  private SavaProduct(): any {
    let input = new FormData();
    input.append('pname', this.addProductForm.get('pname').value);
    input.append('imagePath', this.addProductForm.get('imagePath').value);
    input.append('pcost', this.addProductForm.get('pcost').value);
    input.append('description', this.addProductForm.get('description').value);
    input.append('status', this.addProductForm.get('status').value);
    console.log(input);
    return input;
  }

  onSubmit() {
    const formModel = this.SavaProduct();
    this.Product.addData(formModel)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        }  else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
        res = res.data;
      });
    this.root.navigate(['/adminDashboard/products/listp']);
  }
}
