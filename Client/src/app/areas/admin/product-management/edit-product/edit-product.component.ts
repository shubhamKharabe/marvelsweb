import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ProductsService } from '../../../../services/productServices/products.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {
  editForm: FormGroup;
  id: number;
  localUrl: any;
  userData: any;
  constructor(private Product: ProductsService,
    private route: ActivatedRoute,
    private formBuild: FormBuilder,
    private root: Router, 
    private messageService: ToastrService) { }

  ngOnInit() {
    this.editForm = this.formBuild.group({
      newpname: new FormControl(''),
      newimagePath: null,
      newpcost: new FormControl(''),
      newdescription: new FormControl(''),
      newstatus: new FormControl(''),
    });

    this.route.params.subscribe(params => {
      console.log(params.id);
      this.Product.listProductData(params.id)
        .subscribe((res: any) => {
          if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          }
          this.userData = res;
          console.log(this.userData);
          this.editForm.patchValue({
            newpname: this.userData.pname,
            newpcost: this.userData.pcost,
            newdescription: this.userData.description,
            newstatus: this.userData.status,
          })
          this.localUrl = this.userData.imagePath;
        })
    })
  }
  onUpload(event) {
    if (event.target.files.length > 0) {
      var reader= new FileReader();
      reader.onload=(event: ProgressEvent)=>{
        this.localUrl= (<FileReader>event.target).result;
      ;
      }
      reader.readAsDataURL(event.target.files[0])
      let file = event.target.files[0];
      this.editForm.get('newimagePath').setValue(file);
      console.log(file);
    }
  }
  private SavaEditProduct(): any {
    let input = new FormData();
    input.append('newpname', this.editForm.get('newpname').value);
    input.append('newimagePath', this.editForm.get('newimagePath').value);
    input.append('newpcost', this.editForm.get('newpcost').value);
    input.append('newdescription', this.editForm.get('newdescription').value);
    input.append('newstatus', this.editForm.get('newstatus').value);
    console.log(input);
    return input;
  }
  onSubmit() {
    const formModel = this.SavaEditProduct();
    this.route.params.subscribe(params => {
      console.log(formModel);
      this.Product.editData(formModel, params['id'])
        .subscribe((res: any) => {
          if (res.code == 200) {
            this.messageService.success(res.message);
            res = res.data;
          } else if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          }  else {
            this.messageService.error(res.message);
          }
          res = res.data;
        })
    })
    if (localStorage.token) {
      if (localStorage.role == "admin") {
        this.root.navigate(['adminDashboard/products/listp']);
      }
      else {
        this.root.navigate(['dashboard/products/listp']);
      }
    }
  }
}
