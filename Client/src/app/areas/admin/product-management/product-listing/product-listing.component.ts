import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProductsService } from '../../../../services/productServices/products.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-listing.component.html',
  styleUrls: ['./product-listing.component.scss']
})
export class ProductListingComponent implements OnInit {
  products: any;
  id: number;
  _id: number;
  display: boolean = false;
  constructor(private Product: ProductsService,
    private root: Router,
    private messageService: ToastrService) { }


  ngOnInit() {
    this.getDetails()
  }

  getDetails() {
    this.Product.listData().subscribe(res => {
      if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      }
      this.products = res;
    });
  }

  showDialog(id) {
    this.display = true;
    this._id = id;
    console.log(this._id);
  }

  delete(id) {
    this.display = false;
    console.log("id:", id);
    this.Product.deleteData(id).subscribe(res => {
      if (res.code == 200) {
        this.messageService.success(res.message);
        res = res.data;
      } else if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      } else {
        this.messageService.error(res.message);
      }
    })
    this.getDetails();
  }
}
