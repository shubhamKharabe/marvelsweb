import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddProductComponent } from './add-product/add-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { ProductListingComponent } from './product-listing/product-listing.component';
import { ProductManagementRoutingModule } from './/product-management-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminModule } from '../admin.module';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import {EditorModule} from 'primeng/editor';
import {DialogModule} from 'primeng/dialog';
import {ScrollPanelModule} from 'primeng/scrollpanel';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ProductManagementRoutingModule,
    AdminModule,
    TableModule,
    DropdownModule,
    InputTextModule,
    EditorModule,
    DialogModule,ScrollPanelModule,
  ],
  declarations: [AddProductComponent,
  EditProductComponent,
  ProductListingComponent,
  ]
})
export class ProductManagementModule { }
