import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {path: "", component: DashboardComponent},
  {path: "products", loadChildren: "./product-management/product-management.module#ProductManagementModule" },
  {path: "users", loadChildren: "./user-management/user-management.module#UserManagementModule" },
  {path: "blogs", loadChildren: "./blog-management/blog-management.module#BlogManagementModule" },
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AdminRoutingModule { }
