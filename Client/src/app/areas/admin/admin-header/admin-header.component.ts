import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from '../../../services/userService/users.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {
  id: any;
  data: any;
  uname: any;
  constructor(private User: UsersService,
    private root: Router) { }

  ngOnInit() {
    this.getDetail() ;
  }
  logout() {
    localStorage.clear();
    this.root.navigate(['/'])
  }
  getDetail() {
    var id = localStorage.id;
    this.User.listUser(id)
      .subscribe((res: any) => {
        this.uname= res.uname;
        console.log(res)
      })
  }
  onClick() {
    this.root.navigate(['/adminDashboard/users/editProfile']);
  };
}
