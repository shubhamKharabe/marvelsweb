import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminRoutingModule } from './admin-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [DashboardComponent, AdminHeaderComponent, AdminFooterComponent],
  exports: [
    AdminHeaderComponent,
    AdminFooterComponent,
  ]

})
export class AdminModule { }
