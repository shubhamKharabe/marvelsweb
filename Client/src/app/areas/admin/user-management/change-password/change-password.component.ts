import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../../../services/userService/users.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  id: number;
  user: any;
  userData: any;
  data: any;
  constructor(private User: UsersService,
    private root: Router,
    private messageService: ToastrService) { }

  ngOnInit() {
    this.changePasswordForm = new FormGroup({
      "newuname": new FormControl(''),
      "newpsw": new FormControl('', [Validators.required, Validators.maxLength(5)]),
      "psw2": new FormControl('', [Validators.required, Validators.maxLength(5)]),
      "newbdate": new FormControl(''),
      "newemail": new FormControl(''),
      "newgender": new FormControl(''),
      "newrole": new FormControl(''),
    })
    this.data = { userId: localStorage.getItem("id") }
    // console.log("***********",this.data.token)
    this.User.editUser(this.data)
      .subscribe((res: any) => {
        if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        }
        this.userData = res;
        this.id = res[0]._id
        console.log("=====================", this.userData);
        this.changePasswordForm.patchValue({
          newuname: this.userData[0].uname,
          newbdate: this.userData[0].bdate,
          newemail: this.userData[0].email,
          newgender: this.userData[0].gender,
          newrole: this.userData[0].role,
        })
      })
  }
  onSubmit() {
    console.log(this.changePasswordForm.value);
    if (this.changePasswordForm.value.newpsw == this.changePasswordForm.value.psw2) {
      this.User.edit(this.changePasswordForm.value, this.id)
        .subscribe((res: any) => {
          if (res.code == 200) {
            this.messageService.success(res.message);
            res = res.data;
          }  else if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          } else {
            this.messageService.error(res.message);
          }
          res = res.data;
        })
      if (localStorage.token) {
        if (localStorage.role == "admin") {
          this.root.navigate(['adminDashboard']);
        }
        else {
          this.root.navigate(['dashboard']);
        }
      }

    } else {
      this.messageService.warning("Fields must be same and less than 5 leters.");
    }
  }
}
