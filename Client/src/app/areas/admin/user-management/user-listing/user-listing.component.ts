import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../../../services/userService/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-listing',
  templateUrl: './user-listing.component.html',
  styleUrls: ['./user-listing.component.scss']
})
export class UserListingComponent implements OnInit {
  users: any[];
  id: number;
  _id: number;
  display: boolean = false;
  constructor(private User: UsersService,
    private root: Router,
    private messageService: ToastrService) { }

  ngOnInit() {
    this.getDetails();
  }

  getDetails() {
    this.User.list().subscribe(res => {
      if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      }
        this.users = res;
    });
  }

  showDialog(id) {
    this.display = true;
    this._id = id;
    console.log(this._id);
  }

  delete(id) {
    this.display = false;
    console.log("id:", id);
    this.User.delete(id).subscribe(res => {
      console.log("deleted");
      if (res.code == 200) {
        this.messageService.success(res.message);
        res = res.data;
        this.getDetails();
      } else if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      } else {
        this.messageService.error(res.message);
      }
    })
  }
}
