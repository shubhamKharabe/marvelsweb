import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

const routes: Routes=[
  {path: "listu", component: UserListingComponent},
  {path: "addu", component: UserAddComponent},
  {path: "editu/:id", component: UserEditComponent},
  {path: "editProfile", component: EditAdminProfileComponent},
  {path: "changePassword", component: ChangePasswordComponent},
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class UserManagementRoutingModule { }
