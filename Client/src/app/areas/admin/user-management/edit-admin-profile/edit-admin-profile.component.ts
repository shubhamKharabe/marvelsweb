import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../../../services/userService/users.service';

@Component({
  selector: 'app-edit-admin-profile',
  templateUrl: './edit-admin-profile.component.html',
  styleUrls: ['./edit-admin-profile.component.scss']
})
export class EditAdminProfileComponent implements OnInit {
  editUserForm: FormGroup;
  id: number;
  user: any;
  userData: any;
  data: any;
  constructor(private User: UsersService,
    private root: Router,
    private messageService: ToastrService) { }

  ngOnInit() {
    this.editUserForm = new FormGroup({
      "newuname": new FormControl(''),
      "newpsw": new FormControl(''),
      "newbdate": new FormControl(''),
      "newemail": new FormControl(''),
      "newgender": new FormControl(''),
      "newrole": new FormControl(''),
    })
    this.data = { userId: localStorage.getItem("id") }
    this.User.editUser(this.data)
      .subscribe((res: any) => {
        if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        }
        this.userData = res;
        this.id = res[0]._id
        console.log("=====================", this.userData);
        this.editUserForm.patchValue({
          newuname: this.userData[0].uname,
          newpsw: this.userData[0].psw,
          newbdate: this.userData[0].bdate,
          newemail: this.userData[0].email,
          newgender: this.userData[0].gender,
          newrole: this.userData[0].role,
        })
      })
  }
  onSubmit() {
    console.log(this.editUserForm.value);
    this.User.edit(this.editUserForm.value, this.id)
      .subscribe((res: any) => {
        if (res.code == 200) {
          localStorage.setItem("uname", this.editUserForm.value.newuname);
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
      })
    if (localStorage.token) {
      if (localStorage.role == "admin") {
        this.root.navigate(['adminDashboard/users/listu']);
      }
      else {
        this.root.navigate(['dashboard/users/listu']);
      }
    }
  }
}
