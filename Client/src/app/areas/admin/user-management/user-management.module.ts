import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserListingComponent } from './user-listing/user-listing.component';
import { UserManagementRoutingModule } from './/user-management-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AdminModule } from '../admin.module';
import {CalendarModule} from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import { EditAdminProfileComponent } from './edit-admin-profile/edit-admin-profile.component';
import {DialogModule} from 'primeng/dialog';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  imports: [
    CommonModule,
    UserManagementRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    AdminModule,
    InputTextModule,
    CalendarModule,
    ShowHidePasswordModule.forRoot(),
    DropdownModule,
    TableModule,
    DialogModule,
  ],
  declarations: [UserListingComponent, UserEditComponent, UserAddComponent, EditAdminProfileComponent, ChangePasswordComponent]
})
export class UserManagementModule { }
