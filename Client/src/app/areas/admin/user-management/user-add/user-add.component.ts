import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { Router } from '@angular/router';
import { UsersService } from '../../../../services/userService/users.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {
  addUserForm: FormGroup;
  gender: SelectItem[];
  role: SelectItem[];
  value: Date;

  constructor(private User: UsersService,
    private root: Router,
    private messageService: ToastrService) {
    this.gender = [
      { label: 'Select Gender', value: null },
      { label: 'Male', value: 'male' },
      { label: 'Female', value: 'female' },
      { label: 'Other', value: 'other' },
    ];
    this.role = [
      { label: 'Select Role', value: null },
      { label: 'User', value: 'user' },
      { label: 'Admin', value: 'admin' },
    ];
  }
  ngOnInit() {
    this.addUserForm = new FormGroup({
      "uname": new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(10)]),
      "psw": new FormControl('', [Validators.required, Validators.maxLength(8)]),
      "email": new FormControl('', Validators.compose([Validators.required, Validators.email])),
      "bdate": new FormControl('', [Validators.required]),
      "gender": new FormControl('', [Validators.required]),
      "role": new FormControl(''),
    })
  }
  onSubmit() {
    console.log(this.addUserForm.value);
    if (this.addUserForm.value.bdate > Date.now()) {
      this.messageService.error("Bhoot! Bhoot! Haven't u born yet?");
      return null;
    }
    this.User.add(this.addUserForm.value)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
      });
  }
}
