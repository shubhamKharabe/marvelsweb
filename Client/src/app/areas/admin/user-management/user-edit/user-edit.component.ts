import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UsersService } from '../../../../services/userService/users.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  editUserForm: FormGroup;
  id: number;
  user: any;
  userData: any;
  constructor(private User: UsersService,
    private route: ActivatedRoute,
    private root: Router,
    private messageService: ToastrService) { }

  ngOnInit() {
    this.editUserForm = new FormGroup({
      "newuname": new FormControl(''),
      "newpsw": new FormControl(''),
      "newemail": new FormControl(''),
      "newgender": new FormControl(''),
      "newrole": new FormControl(''),
      "newbdate": new FormControl(''),
    })

    this.route.params.subscribe(params => {
      console.log(params.id);
      this.User.listUser(params.id)
        .subscribe((res: any) => {
          if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          }
          this.userData = res;
          console.log(this.userData);
          this.editUserForm.patchValue({
            newuname: this.userData.uname,
            newpsw: this.userData.psw,
            newemail: this.userData.email,
            newgender: this.userData.gender,
            newrole: this.userData.role,
            newbdate: this.userData.bdate,
          })
        })
    })
  }


  onSubmit() {
    this.route.params.subscribe(params => {
      console.log(this.editUserForm.value);
      this.User.edit(this.editUserForm.value, params['id'])
        .subscribe((res: any) => {
          if (res.code == 200) {
            this.messageService.success(res.message);
            res = res.data;
          } else if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          }  else {
            this.messageService.error(res.message);
          }
          res = res.data;
        })
    })
    if (localStorage.token) {
      if (localStorage.role == "admin") {
        this.root.navigate(['adminDashboard/users/listu']);
      }
      else {
        this.root.navigate(['dashboard/users/listu']);
      }
    }
  }
}
