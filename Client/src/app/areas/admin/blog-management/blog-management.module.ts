import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogManagementRoutingModule } from './/blog-management-routing.module';
import { AddBlogComponent } from './add-blog/add-blog.component';
import { ListBlogComponent } from './list-blog/list-blog.component';
import { AdminModule } from '../admin.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import {DropdownModule} from 'primeng/dropdown';
import {TableModule} from 'primeng/table';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {EditorModule} from 'primeng/editor';
import {RatingModule} from 'primeng/rating';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {DialogModule} from 'primeng/dialog';
import { EditBlogComponent } from './edit-blog/edit-blog.component';
@NgModule({
  imports: [
    CommonModule,
    BlogManagementRoutingModule,
    FormsModule,
    AdminModule,
    ReactiveFormsModule,
    InputTextModule,
    EditorModule,
    CalendarModule,
    ShowHidePasswordModule.forRoot(),
    DropdownModule,
    InputTextareaModule,
    RatingModule,
    TableModule,
    ScrollPanelModule,
    DialogModule
  ],
  declarations: [AddBlogComponent, ListBlogComponent, EditBlogComponent]
})
export class BlogManagementModule { }
