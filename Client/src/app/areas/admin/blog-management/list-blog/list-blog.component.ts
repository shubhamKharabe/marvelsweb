import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../../../../services/blogServices/blog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.scss']
})
export class ListBlogComponent implements OnInit {
  addCommentForm: FormGroup;
  blogs: any;
  BlogId: any;
  comments: any;
  data: any;
  displayComments: boolean = false;
  displayDailog: boolean = false;
  id: number;
  _id: number;
  constructor(private Blog: BlogService,
    private root: Router,
    private messageService: ToastrService) { }


  ngOnInit() {
    this.getDetails();
    this.addCommentForm = new FormGroup({
      "comment": new FormControl('', [Validators.required, Validators.minLength(1)]),
    })
  }

  getDetails() {
    this.Blog.listBlogs().subscribe(res => {
        this.blogs = res;  
        if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        }
    });
  }

  delete(id) {
    this.displayDailog = false;
    this.Blog.deleteBlog(id).subscribe(res => {
      if (res.code == 200) {
        this.messageService.success(res.message);
        res = res.data;
      } else if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      } else {
        this.messageService.error(res.message);
      }
    })
    this.getDetails();
  }

  showComments(id) {
    this.Blog.getComments(id).subscribe(res => {
      this.comments = res.comments;
      this.BlogId = id
    });
    this.displayComments = true;
  }
  showDailog(id) {
    this._id = id;
    this.displayDailog = true;
  }
  onSubmit(id) {
    if (!this.addCommentForm.value.comment) { return null }
    this.data = {
      userId: localStorage.getItem("uname"),
      blogId: id,
      comment: this.addCommentForm.value.comment,
      rating: this.addCommentForm.value.rating | 0,
    }
    this.Blog.addComment(this.data)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
        res = res.data;
      });
    this.showComments(id);
    this.addCommentForm.reset();
    this.displayComments = false;
  }
}
