import { BlogManagementRoutingModule } from './blog-management-routing.module';

describe('BlogManagementRoutingModule', () => {
  let blogManagementRoutingModule: BlogManagementRoutingModule;

  beforeEach(() => {
    blogManagementRoutingModule = new BlogManagementRoutingModule();
  });

  it('should create an instance', () => {
    expect(blogManagementRoutingModule).toBeTruthy();
  });
});
