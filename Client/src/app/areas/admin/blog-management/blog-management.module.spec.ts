import { BlogManagementModule } from './blog-management.module';

describe('BlogManagementModule', () => {
  let blogManagementModule: BlogManagementModule;

  beforeEach(() => {
    blogManagementModule = new BlogManagementModule();
  });

  it('should create an instance', () => {
    expect(blogManagementModule).toBeTruthy();
  });
});
