import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../../../../services/blogServices/blog.service';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.scss']
})
export class AddBlogComponent implements OnInit {
  addBlogForm: FormGroup;
  value: Date;
  constructor(private Blog: BlogService,
    private formBuild: FormBuilder,
    private root: Router,
    private messageService: ToastrService) { }


  ngOnInit() {
    this.addBlogForm = this.formBuild.group({
      title: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      img: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required, Validators.minLength(150)]),
      date: new FormControl('', [Validators.required]),
    });
  }

  onUpload(event) {
    if (event.target.files.length > 0) {
      let file = event.target.files[0];
      this.addBlogForm.get('img').setValue(file);
      console.log(file);
    }
  }
  private SavaProduct(): any {
    let input = new FormData();
    input.append('title', this.addBlogForm.get('title').value);
    input.append('img', this.addBlogForm.get('img').value);
    input.append('description', this.addBlogForm.get('description').value);
    input.append('date', this.addBlogForm.get('date').value);
    console.log(input);
    return input;
  }

  onSubmit() {
    const formModel = this.SavaProduct();
    this.Blog.addBlog(formModel)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
        res = res.data;
      });
    this.root.navigate(['/adminDashboard/blogs/listb']);
  }
}
