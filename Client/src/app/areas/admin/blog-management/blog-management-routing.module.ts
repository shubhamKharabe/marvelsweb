import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBlogComponent } from './add-blog/add-blog.component';
import { ListBlogComponent } from './list-blog/list-blog.component';
import { Routes, RouterModule } from '@angular/router';
import { EditBlogComponent } from './edit-blog/edit-blog.component';


const routes: Routes=[
  {path: "addb", component: AddBlogComponent },
  {path: "listb", component: ListBlogComponent},
  {path: "editb/:id", component: EditBlogComponent},
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: []
})

export class BlogManagementRoutingModule { }