import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../../../../services/blogServices/blog.service';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.scss']
})
export class EditBlogComponent implements OnInit {
  editForm: FormGroup;
  id: number;
  localUrl: any;
  userData: any;
  constructor(private Blog: BlogService,
    private route: ActivatedRoute,
    private formBuild: FormBuilder,
    private root: Router,
    private messageService: ToastrService) { }

  ngOnInit() {
    this.editForm = this.formBuild.group({
      newtitle: new FormControl(''),
      newimg: null,
      newdescription: new FormControl(''),
    });

    this.route.params.subscribe(params => {
      console.log(params.id);
      this.Blog.getBlog(params.id)
        .subscribe((res: any) => {
          if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          }
          this.userData = res;
          console.log(this.userData);
          this.editForm.patchValue({
            newtitle: this.userData.title,
            newdescription: this.userData.description,
          })
          this.localUrl = this.userData.img;
        })
    })
  }
  onUpload(event) {
    if (event.target.files.length > 0) {
      var reader = new FileReader();
      reader.onload = (event: ProgressEvent) => {
        this.localUrl = (<FileReader>event.target).result;
        ;
      }
      reader.readAsDataURL(event.target.files[0])
      let file = event.target.files[0];
      this.editForm.get('newimg').setValue(file);
      console.log(file);
    }
  }
  private SavaEditProduct(): any {
    let input = new FormData();
    input.append('newtitle', this.editForm.get('newtitle').value);
    input.append('newimg', this.editForm.get('newimg').value);
    input.append('newdescription', this.editForm.get('newdescription').value);
    console.log(input);
    return input;
  }
  onSubmit() {
    const formModel = this.SavaEditProduct();
    this.route.params.subscribe(params => {
      console.log(formModel);
      this.Blog.editData(formModel, params['id'])
        .subscribe((res: any) => {
          if (res.code == 200) {
            this.messageService.success(res.message);
            res = res.data;
          } else if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          } else {
            this.messageService.error(res.message);
          }
          res = res.data;
        })
    })
    if (localStorage.token) {
      if (localStorage.role == "admin") {
        this.root.navigate(['adminDashboard/blogs/listb']);
      }
      else {
        this.root.navigate(['dashboard/blogs/listb']);
      }
    }
  }
}
