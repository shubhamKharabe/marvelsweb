import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-header',
  templateUrl: './user-header.component.html',
  styleUrls: ['./user-header.component.scss']
})
export class UserHeaderComponent implements OnInit {
  uname = localStorage.uname;
  id: any;
  data: any;

  constructor(
    private root: Router,) { }

  ngOnInit() {
  }
  logout() {
    localStorage.clear();
    this.root.navigate(['/'])
  }
}

