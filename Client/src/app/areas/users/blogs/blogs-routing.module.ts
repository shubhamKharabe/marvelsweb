import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListBlogComponent } from './list-blog/list-blog.component';

const routes: Routes=[
  {path: "listb", component: ListBlogComponent},
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: []
})
export class BlogsRoutingModule { }
