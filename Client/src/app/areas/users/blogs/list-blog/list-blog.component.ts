import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { BlogService } from '../../../../services/blogServices/blog.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-blog',
  templateUrl: './list-blog.component.html',
  styleUrls: ['./list-blog.component.scss']
})
export class ListBlogComponent implements OnInit {
  addCommentForm: FormGroup;
  data: any;
  blogs: any;
  comments: any;
  id: number;
  rating: number;
  constructor(
    private Blog: BlogService,
    private messageService: ToastrService,
    private root: Router,
  ) { }


  ngOnInit() {
    this.getBlogs();
    this.addCommentForm = new FormGroup({
      "comment": new FormControl(''),
      "rating": new FormControl(''),
    })
  }

  getBlogs() {
    this.Blog.listBlogs().subscribe(res => {
      if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      }
      this.blogs = res;
    });
  }
  onSubmit(id) {
    if (!this.addCommentForm.value.comment) { return null }
    console.log("***********", id)
    this.data = {
      userId: localStorage.getItem("uname"),
      blogId: id,
      comment: this.addCommentForm.value.comment,
      rating: this.addCommentForm.value.rating | 0,
    }
    console.log("***********", this.data)
    this.Blog.addComment(this.data)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
        res = res.data;
      });
    this.getBlogs();
    this.addCommentForm.reset();
    // location.reload();
  }
}
