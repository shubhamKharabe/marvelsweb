import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListBlogComponent } from './list-blog/list-blog.component';
import { BlogsRoutingModule } from './blogs-routing.module';
import {CardModule} from 'primeng/card';
import {RatingModule} from 'primeng/rating';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DialogModule} from 'primeng/dialog';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {InputTextareaModule} from 'primeng/inputtextarea';
import { UsersModule } from '../users.module';
@NgModule({
  imports: [
    CommonModule,
    BlogsRoutingModule,
    CardModule,
    RatingModule,
    FormsModule,
    DialogModule,
    ReactiveFormsModule,
    ScrollPanelModule,
    InputTextareaModule,
    UsersModule,
  ],
  declarations: [ListBlogComponent]
})
export class BlogsModule { }
