import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersRoutingModule } from './users-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserHeaderComponent } from './user-header/user-header.component';
import { BsDropdownModule } from 'ngx-bootstrap';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { UserFooterComponent } from './user-footer/user-footer.component';
import { ChangeUserPasswordComponent } from './change-user-password/change-user-password.component';
import {InputTextModule} from 'primeng/inputtext';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    ReactiveFormsModule,
    InputTextModule,
    FormsModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [DashboardComponent, 
    UserHeaderComponent, 
    EditUserProfileComponent,
    UserFooterComponent,
    UserFooterComponent,
    ChangeUserPasswordComponent,
    ],
  exports: [
    UserHeaderComponent,
    UserFooterComponent,
  ]
})
export class UsersModule { }
