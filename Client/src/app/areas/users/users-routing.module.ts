import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { ChangeUserPasswordComponent } from './change-user-password/change-user-password.component';

const routes: Routes = [
  {path: "", component: DashboardComponent},
  {path: "product", loadChildren: "./products/products.module#ProductsModule" },
  {path: "blog", loadChildren: "./blogs/blogs.module#BlogsModule" },
  {path: "users/editProfile", component: EditUserProfileComponent },
  {path: "users/changePassword", component: ChangeUserPasswordComponent },
]
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class UsersRoutingModule { }
