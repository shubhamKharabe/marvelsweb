import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ProductsService } from '../../../../services/productServices/products.service';
import { CartServiceService } from '../../../../services/cartServices/cart-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-listing',
  templateUrl: './product-listing.component.html',
  styleUrls: ['./product-listing.component.scss']
})
export class ProductListingComponent implements OnInit {
  products: any;
  id: number;
  cost: number;
  data: object;
  constructor(
    private product: ProductsService,
    private Cart: CartServiceService,
    private root: Router,
    private messageService: ToastrService) { }


  ngOnInit() {
    this.getDetails()
  }

  getDetails() {
    this.product.listData().subscribe(res => {
      if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      }
      this.products = res;
    });
  }
  addToCart(productId, productCost) {
    this.data = {
      userId: localStorage.getItem("id"),
      productId: productId,
      productCost: productCost,
      quantity: 1,
    }
    console.log("***********", this.data)
    this.Cart.addToCart(this.data)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
      });
  }
}
