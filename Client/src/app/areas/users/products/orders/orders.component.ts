import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../../../services/ordersService/orders.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(
    private Orders: OrdersService,
    private messageService: ToastrService,
    private root: Router) { }

  id: string;
  data: string;
  ngOnInit() {
    this.getDetails();
  }
  getDetails() {
    this.id = localStorage.getItem("id"),
      console.log("ID:", this.id)
    this.Orders.listOrders(this.id).subscribe(res => {
      if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      }
      this.data = res;
      console.log("sadsad", this.data)
    });
  }
}
