import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsRoutingModule } from './/products-routing.module';
import { ProductListingComponent } from './product-listing/product-listing.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {TableModule} from 'primeng/table';
import {CarouselModule} from 'primeng/carousel';
import {CardModule} from 'primeng/card';
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {SpinnerModule} from 'primeng/spinner';
import { CartComponent } from './cart/cart.component';
import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import { UsersModule } from '../users.module';
import { OrdersComponent } from './orders/orders.component';
@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    TableModule,
    CarouselModule,
    InputTextModule,
    CardModule,
    ScrollPanelModule,
    SpinnerModule,
    DialogModule,
    DropdownModule,
    UsersModule,
  ],
  declarations: [ProductListingComponent, CartComponent, OrdersComponent,
  ]
})
export class ProductsModule { }
