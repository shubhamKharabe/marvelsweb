import { Component, OnInit, AfterViewInit, Input, ViewChild, ElementRef, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { PaymentService } from '../../../../services/payment.service';
import { ToastrService } from 'ngx-toastr';
import { CartServiceService } from '../../../../services/cartServices/cart-service.service';
import { ProductsService } from '../../../../services/productServices/products.service';
import { OrdersService } from '../../../../services/ordersService/orders.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
// , AfterViewInit, OnDestroy 
export class CartComponent implements OnInit, AfterViewInit, OnDestroy {

  carts: any;
  products: any;
  id: string;
  cartId: string;
  sum: number;
  data: any;
  paymentType: any;
  subtotal: number;
  display: boolean = false;
  placeOrder: boolean = false;
  deleteFromCart: boolean = false;
  addressForm: FormGroup;
  city: SelectItem[];
  paymentMethod: SelectItem[];
  disabled: boolean = true;
  address: string;

  // @Input() amount: number;
  // @Input() label: string;
  // elements: any;
  // paymentRequest: any;
  // prButton: any;
  // emailAddress: any;

  // @ViewChild('payElement') payElement;

  constructor(
    private payment: PaymentService,
    private cd: ChangeDetectorRef,
    private Order: OrdersService,
    private Product: ProductsService,
    private Cart: CartServiceService,
    private messageService: ToastrService,
    private root: Router) {
    this.city = [
      { label: 'Select City', value: null },
      { label: 'Nagpur', value: 'nagpur' },
      { label: 'Pune', value: 'pune' },
      { label: 'Mumbai', value: 'mumbai' },
    ];
    // this.paymentMethod = [
    //   { label: 'Select Payment', value: null },
    //   { label: 'Cash On Delivery', value: 'cod' },
    //   { label: 'Debit/Credit', value: 'd/c' }
    // ];
  }

  ngOnInit() {
    this.getDetails();
    this.getProducts();

    this.addressForm = new FormGroup({
      "street": new FormControl('', [Validators.required, Validators.maxLength(50)]),
      "city": new FormControl('', [Validators.required]),
      "pincode": new FormControl('', [Validators.required]),
      "state": new FormControl('maharashtra'),
      "country": new FormControl('india')
    })
  }

  @ViewChild('cardInfo') cardInfo: ElementRef;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }
  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      this.data = {
        amount: this.sum,
        stripeToken: token.id,
        userId: this.id
      }
      this.payment.done(this.data)
        .subscribe((res: any) => {
          if (res.code == 200) {
            this.messageService.success(res.message);
            this.getDetails();
            this.addAddress()
            this.deleteAllCart(this.id);
            this.placeOrder = false;
            res = res.data;
          } else if (res.code == 401) {
            localStorage.clear();
            this.messageService.error(res.message);
            this.root.navigate(['/login']);
          } else {
            this.messageService.error(res.message);
          }
        });
      // ...send the token to the your backend to process the charge
    }
  }

  getDetails() {
    this.id = localStorage.getItem("id"),
      this.Cart.listData(this.id).subscribe(res => {
        if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        }
        this.carts = res;

        this.sum = 0;
        for (var i = 0; i < res.length; i++) {
          this.sum += res[i].total;
        }
      });
  }
  getProducts() {
    this.Product.listData().subscribe(res => {
      this.products = res;
    });
  }
  addToCart(productId, productCost) {
    this.data = {
      userId: localStorage.getItem("id"),
      productId: productId,
      productCost: productCost,
      quantity: 1,
    }
    this.Cart.addToCart(this.data)
      .subscribe((res: any) => {
        if (res.code == 200) {
          this.messageService.success(res.message);
          res = res.data;
        } else if (res.code == 401) {
          localStorage.clear();
          this.messageService.error(res.message);
          this.root.navigate(['/login']);
        } else {
          this.messageService.error(res.message);
        }
        this.getDetails();
      });
  }

  updateCart(pId, pCost, pQty) {
    let data = {
      userId: localStorage.getItem("id"),
      productId: pId,
      productCost: pCost,
      quantity: pQty,
    }
    this.Cart.editCart(data)
      .subscribe((res: any) => {
        this.getDetails();
      });
  }
  deleteCart(cartId) {
    this.Cart.deleteData(cartId).subscribe(res => {
      if (res.code == 200) {
        this.messageService.success(res.message);
        res = res.data;
      } else if (res.code == 401) {
        localStorage.clear();
        this.messageService.error(res.message);
        this.root.navigate(['/login']);
      } else {
        this.messageService.error(res.message);
      }
      this.getDetails();
      this.deleteFromCart = false;
    });
  }

  deleteAllCart(id) {
    this.Cart.DeleteAllCart(id).subscribe(res => {
    });
  }

  showDialog() {
    if (this.sum > 0) {
      this.display = true;
    } else {
      this.messageService.warning("Cart Is Empty!");
    }
  }
  showDeleteFromCart(id) {
    this.deleteFromCart = true;
    this.cartId = id;
  }
  showPlaceOrder() {
    this.display = false;
    this.placeOrder = true;
  }
  addAddress() {
    this.address = "Country:'" + this.addressForm.value.country +
      "', State:'" + this.addressForm.value.state +
      "', City:'" + this.addressForm.value.city +
      "', Pincode:'" + this.addressForm.value.pincode +
      "', Street:'" + this.addressForm.value.street + "'.";
    this.addressForm.reset();
    this.display = false;
    this.placeOrder = true;
    let data = {
      total: this.sum,
      userId: this.id,
      address: this.address,
    }
    this.Order.addToOrders(data)
      .subscribe((res: any) => {
        res = res;
        this.getDetails();
      });

  }
}