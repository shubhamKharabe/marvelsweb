(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["products-products-module"],{

/***/ "./node_modules/primeng/components/spinner/spinner.js":
/*!************************************************************!*\
  !*** ./node_modules/primeng/components/spinner/spinner.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var common_1 = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var inputtext_1 = __webpack_require__(/*! ../inputtext/inputtext */ "./node_modules/primeng/components/inputtext/inputtext.js");
var domhandler_1 = __webpack_require__(/*! ../dom/domhandler */ "./node_modules/primeng/components/dom/domhandler.js");
var forms_1 = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
exports.SPINNER_VALUE_ACCESSOR = {
    provide: forms_1.NG_VALUE_ACCESSOR,
    useExisting: core_1.forwardRef(function () { return Spinner; }),
    multi: true
};
var Spinner = /** @class */ (function () {
    function Spinner(el, domHandler) {
        this.el = el;
        this.domHandler = domHandler;
        this.onChange = new core_1.EventEmitter();
        this.onFocus = new core_1.EventEmitter();
        this.onBlur = new core_1.EventEmitter();
        this.step = 1;
        this.decimalSeparator = '.';
        this.thousandSeparator = ',';
        this.formatInput = true;
        this.type = 'text';
        this.valueAsString = '';
        this.onModelChange = function () { };
        this.onModelTouched = function () { };
        this.keyPattern = /[0-9\+\-]/;
        this.negativeSeparator = '-';
    }
    Spinner.prototype.ngOnInit = function () {
        if (Math.floor(this.step) === 0) {
            this.precision = this.step.toString().split(/[,]|[.]/)[1].length;
        }
    };
    Spinner.prototype.repeat = function (event, interval, dir) {
        var _this = this;
        var i = interval || 500;
        this.clearTimer();
        this.timer = setTimeout(function () {
            _this.repeat(event, 40, dir);
        }, i);
        this.spin(event, dir);
    };
    Spinner.prototype.spin = function (event, dir) {
        var step = this.step * dir;
        var currentValue = this.value || 0;
        var newValue = null;
        if (this.precision)
            this.value = parseFloat(this.toFixed(currentValue + step, this.precision));
        else
            this.value = currentValue + step;
        if (this.maxlength !== undefined && this.value.toString().length > this.maxlength) {
            this.value = currentValue;
        }
        if (this.min !== undefined && this.value < this.min) {
            this.value = this.min;
        }
        if (this.max !== undefined && this.value > this.max) {
            this.value = this.max;
        }
        this.formatValue();
        this.onModelChange(this.value);
        this.onChange.emit(event);
    };
    Spinner.prototype.toFixed = function (value, precision) {
        var power = Math.pow(10, precision || 0);
        return String(Math.round(value * power) / power);
    };
    Spinner.prototype.onUpButtonMousedown = function (event) {
        if (!this.disabled) {
            this.inputfieldViewChild.nativeElement.focus();
            this.repeat(event, null, 1);
            this.updateFilledState();
            event.preventDefault();
        }
    };
    Spinner.prototype.onUpButtonMouseup = function (event) {
        if (!this.disabled) {
            this.clearTimer();
        }
    };
    Spinner.prototype.onUpButtonMouseleave = function (event) {
        if (!this.disabled) {
            this.clearTimer();
        }
    };
    Spinner.prototype.onDownButtonMousedown = function (event) {
        if (!this.disabled) {
            this.inputfieldViewChild.nativeElement.focus();
            this.repeat(event, null, -1);
            this.updateFilledState();
            event.preventDefault();
        }
    };
    Spinner.prototype.onDownButtonMouseup = function (event) {
        if (!this.disabled) {
            this.clearTimer();
        }
    };
    Spinner.prototype.onDownButtonMouseleave = function (event) {
        if (!this.disabled) {
            this.clearTimer();
        }
    };
    Spinner.prototype.onInputKeydown = function (event) {
        if (event.which == 38) {
            this.spin(event, 1);
            event.preventDefault();
        }
        else if (event.which == 40) {
            this.spin(event, -1);
            event.preventDefault();
        }
    };
    Spinner.prototype.onInputKeyPress = function (event) {
        var inputChar = String.fromCharCode(event.charCode);
        if (!this.keyPattern.test(inputChar) && inputChar != this.decimalSeparator && event.keyCode != 9 && event.keyCode != 8 && event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 46) {
            event.preventDefault();
        }
    };
    Spinner.prototype.onInputKeyup = function (event) {
        this.decimalSeparatorRegEx = this.decimalSeparatorRegEx || new RegExp(this.decimalSeparator === '.' ? '\\.' : this.decimalSeparator, "g");
        var inputValue = event.target.value.trim();
        this.value = this.parseValue(inputValue);
        if (this.shouldFormat(inputValue)) {
            this.formatValue();
        }
        this.onModelChange(this.value);
        this.updateFilledState();
    };
    Spinner.prototype.shouldFormat = function (value) {
        if (this.negativeSeparator === value) {
            return false;
        }
        if (!this.domHandler.isInteger(this.step) && (value.match(this.decimalSeparatorRegEx) || []).length === 1 && value.indexOf(this.decimalSeparator) === value.length - 1) {
            return false;
        }
        return true;
    };
    Spinner.prototype.onInputBlur = function (event) {
        this.focus = false;
        this.restrictValue();
        this.onModelTouched();
        this.onBlur.emit(event);
    };
    Spinner.prototype.onInputFocus = function (event) {
        this.focus = true;
        this.onFocus.emit(event);
    };
    Spinner.prototype.parseValue = function (val) {
        var value;
        if (this.formatInput) {
            val = val.split(this.thousandSeparator).join('');
        }
        if (val.trim() === '') {
            value = null;
        }
        else {
            if (this.precision) {
                value = parseFloat(val.replace(',', '.'));
            }
            else {
                value = parseInt(val);
            }
            if (isNaN(value)) {
                value = null;
            }
        }
        return value;
    };
    Spinner.prototype.restrictValue = function () {
        var restricted;
        if (this.max !== undefined && this.value > this.max) {
            this.value = this.max;
            restricted = true;
        }
        if (this.min !== undefined && this.value < this.min) {
            this.value = this.min;
            restricted = true;
        }
        if (restricted) {
            this.onModelChange(this.value);
            this.formatValue();
        }
    };
    Spinner.prototype.formatValue = function () {
        if (this.value !== null && this.value !== undefined) {
            var textValue = String(this.value).replace('.', this.decimalSeparator);
            if (this.formatInput) {
                var parts = textValue.split(this.decimalSeparator);
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandSeparator);
                textValue = parts.join(this.decimalSeparator);
            }
            this.valueAsString = textValue;
        }
        else {
            this.valueAsString = '';
        }
        this.inputfieldViewChild.nativeElement.value = this.valueAsString;
    };
    Spinner.prototype.handleChange = function (event) {
        this.onChange.emit(event);
    };
    Spinner.prototype.clearTimer = function () {
        if (this.timer) {
            clearInterval(this.timer);
        }
    };
    Spinner.prototype.writeValue = function (value) {
        this.value = value;
        this.formatValue();
        this.updateFilledState();
    };
    Spinner.prototype.registerOnChange = function (fn) {
        this.onModelChange = fn;
    };
    Spinner.prototype.registerOnTouched = function (fn) {
        this.onModelTouched = fn;
    };
    Spinner.prototype.setDisabledState = function (val) {
        this.disabled = val;
    };
    Spinner.prototype.updateFilledState = function () {
        this.filled = (this.value !== undefined && this.value != null);
    };
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Spinner.prototype, "onChange", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Spinner.prototype, "onFocus", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], Spinner.prototype, "onBlur", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], Spinner.prototype, "step", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], Spinner.prototype, "min", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], Spinner.prototype, "max", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], Spinner.prototype, "maxlength", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], Spinner.prototype, "size", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "placeholder", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "inputId", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Spinner.prototype, "disabled", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Spinner.prototype, "readonly", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "decimalSeparator", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "thousandSeparator", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], Spinner.prototype, "tabindex", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Spinner.prototype, "formatInput", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "type", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Boolean)
    ], Spinner.prototype, "required", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "name", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "inputStyle", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], Spinner.prototype, "inputStyleClass", void 0);
    __decorate([
        core_1.ViewChild('inputfield'),
        __metadata("design:type", core_1.ElementRef)
    ], Spinner.prototype, "inputfieldViewChild", void 0);
    Spinner = __decorate([
        core_1.Component({
            selector: 'p-spinner',
            template: "\n        <span class=\"ui-spinner ui-widget ui-corner-all\">\n            <input #inputfield [attr.type]=\"type\" [attr.id]=\"inputId\" [value]=\"valueAsString\" [attr.name]=\"name\"\n            [attr.size]=\"size\" [attr.maxlength]=\"maxlength\" [attr.tabindex]=\"tabindex\" [attr.placeholder]=\"placeholder\" [disabled]=\"disabled\" [attr.readonly]=\"readonly\" [attr.required]=\"required\"\n            (keydown)=\"onInputKeydown($event)\" (keyup)=\"onInputKeyup($event)\" (keypress)=\"onInputKeyPress($event)\" (blur)=\"onInputBlur($event)\" (change)=\"handleChange($event)\" (focus)=\"onInputFocus($event)\"\n            [ngStyle]=\"inputStyle\" [class]=\"inputStyleClass\" [ngClass]=\"'ui-spinner-input ui-inputtext ui-widget ui-state-default ui-corner-all'\">\n            <button type=\"button\" [ngClass]=\"{'ui-spinner-button ui-spinner-up ui-corner-tr ui-button ui-widget ui-state-default':true,'ui-state-disabled':disabled}\" [disabled]=\"disabled\" [attr.readonly]=\"readonly\"\n                (mouseleave)=\"onUpButtonMouseleave($event)\" (mousedown)=\"onUpButtonMousedown($event)\" (mouseup)=\"onUpButtonMouseup($event)\">\n                <span class=\"ui-spinner-button-icon pi pi-caret-up ui-clickable\"></span>\n            </button>\n            <button type=\"button\" [ngClass]=\"{'ui-spinner-button ui-spinner-down ui-corner-br ui-button ui-widget ui-state-default':true,'ui-state-disabled':disabled}\" [disabled]=\"disabled\" [attr.readonly]=\"readonly\"\n                (mouseleave)=\"onDownButtonMouseleave($event)\" (mousedown)=\"onDownButtonMousedown($event)\" (mouseup)=\"onDownButtonMouseup($event)\">\n                <span class=\"ui-spinner-button-icon pi pi-caret-down ui-clickable\"></span>\n            </button>\n        </span>\n    ",
            host: {
                '[class.ui-inputwrapper-filled]': 'filled',
                '[class.ui-inputwrapper-focus]': 'focus'
            },
            providers: [domhandler_1.DomHandler, exports.SPINNER_VALUE_ACCESSOR]
        }),
        __metadata("design:paramtypes", [core_1.ElementRef, domhandler_1.DomHandler])
    ], Spinner);
    return Spinner;
}());
exports.Spinner = Spinner;
var SpinnerModule = /** @class */ (function () {
    function SpinnerModule() {
    }
    SpinnerModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, inputtext_1.InputTextModule],
            exports: [Spinner],
            declarations: [Spinner]
        })
    ], SpinnerModule);
    return SpinnerModule;
}());
exports.SpinnerModule = SpinnerModule;
//# sourceMappingURL=spinner.js.map

/***/ }),

/***/ "./node_modules/primeng/spinner.js":
/*!*****************************************!*\
  !*** ./node_modules/primeng/spinner.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* Shorthand */

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! ./components/spinner/spinner */ "./node_modules/primeng/components/spinner/spinner.js"));

/***/ }),

/***/ "./src/app/areas/users/products/cart/cart.component.html":
/*!***************************************************************!*\
  !*** ./src/app/areas/users/products/cart/cart.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-user-header></app-user-header>\n\n\n<div class=\"container\">\n  <br>\n  <br>\n  <div class=\"row\">\n    <div class=\"col-md-9\">\n      <h2>Shopping Cart</h2>\n      <table class=\"table\">\n        <thead class=\"thead-light\">\n          <tr>\n            <th></th>\n            <th></th>\n            <th>Price</th>\n            <th>Quantity</th>\n            <th>Subtotal</th>\n          </tr>\n        </thead>\n        <tbody>\n          <tr *ngFor=\"let cart of carts\">\n            <td style=\"width: 106px\">\n              <img src=\"{{cart.productId.imagePath}}\" alt=\"\" width=\"100px\" height=\"100px\">\n            </td>\n            <td>\n              <div style=\"color: rgb(17, 77, 85);font-size: 20px\">{{cart.productId.pname | uppercase}}</div>\n              <span style=\"color: rgb(9, 104, 9); font-size: 10px\">\n                {{cart.productId.status | uppercase}}</span>\n              <br>\n              <br>\n              <button type=\"button\" class=\"btn btn-outline-dark btn-sm\" (click)=\"deleteCart(cart._id)\">\n                <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n              </button>\n            </td>\n            <td>\n              Rs.{{cart.productId.pcost}}\n            </td>\n            <td style=\"width: 80px\">\n              <p-spinner size=\"5\" [(ngModel)]=\"cart.quantity\" [min]=\"1\" (onChange)=\"updateCart(cart.productId._id,cart.productId.pcost,cart.quantity)\"></p-spinner>\n              <!-- {{cart.quantity}} -->\n            </td>\n            <td style=\"width: 120px;color: red\">\n              Rs.{{cart.total}}\n            </td>\n          </tr>\n          <tr>\n            <th></th>\n            <th></th>\n            <th></th>\n            <th>Total:</th>\n            <th style=\"width: 120px;color: #4ec67f\">Rs. {{sum}}</th>\n          </tr>\n        </tbody>\n      </table>\n      <div>\n        <br>\n        <br>\n        <p style=\"color: rgb(17, 77, 85);font-size: 13px\">\n          The price and availability of items at Marvels.in are subject to change. The shopping cart is a temporary place to store\n          a list of your items and reflects each item's most recent price. Do you have a promotional code? We'll ask you\n          to enter your claim code when it's time to pay.</p>\n      </div>\n    </div>\n    <div class=\"col-md-3\">\n      <br>\n      <br>\n      <br>\n      <br>\n      <div style=\"border: 1px solid rgb(214, 214, 214); padding: 10px;\">\n        <p style=\"color: rgb(9, 104, 9); font-size: 13px\"> Part of your order qualifies for FREE Delivery</p>\n        <hr>\n        <span style=\"color: rgb(17, 77, 85);font-size: 20px\">Total:</span>\n        <span style=\"color:black;font-size: 20px\"> Rs. {{sum}}</span>\n        <br>\n        <button type=\"button\" class=\"btn btn-login btn-md\" (click)=\"showDialog()\">\n          Proceed to Checkout\n          <i class=\"fa fa-plane\" aria-hidden=\"true\"></i>\n        </button>\n      </div>\n      <!-- <div>\n        <img src=\"assets/images/verticalMarvel.jpg\" style=\"width: 255px;height: 535px;\" alt=\"\">\n      </div> -->\n    </div>\n  </div>\n</div>\n<div class=\"container\">\n  <div class=\"section-title style2\" style=\"font-family: 'Source Sans Pro', sans-serif;padding: 10px;\">\n    <br>\n    <h3>Customers who bought items in your Recent History also bought</h3>\n    <br>\n  </div>\n  <div class=\"p-carousel\">\n    <p-carousel [value]=\"products\" numVisible=\"4\" autoplayInterval=\"10500\">\n      <ng-template let-product pTemplate=\"cards\">\n        <p-card title=\"{{product.pname | uppercase}}\" subtitle=\"Rs. {{product.pcost}}\" [style]=\"{width: '100%', height: '520px'}\"\n          styleClass=\"ui-card-shadow\">\n          <img src=\"{{product.imagePath}}\" alt=\"\" width=\"100%\" height=\"200px\" style=\"border-radius: 15%\">\n          <br>\n          <br>\n          <div style=\"height: 50%;\">\n            <p-scrollPanel [style]=\"{width: '100%', height: '73px'}\">\n              <div style=\"padding: 5px;word-wrap:break-word\" [innerHTML]=\"product.description\"></div>\n            </p-scrollPanel>\n          </div>\n          <p-footer>\n            <div class=\"section-title style2 text-center\" style=\"color: black;padding: 5px\">\n              XL/ XXL/ S\n            </div>\n            <div class=\"section-title style2 text-center\">\n              <button button type=\"button\" class=\"btn btn-login\" (click)=addToCart(product._id,product.pcost)>Add to Cart</button>\n            </div>\n          </p-footer>\n        </p-card>\n        <br>\n      </ng-template>\n    </p-carousel>\n    <br>\n    <br>\n  </div>\n</div>\n\n<div style=\"height: 100%\">\n  <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('/assets/images/1.jpg');background-repeat: no-repeat;  background-position: center;background-size: cover;width: 100%;height: 100%;background-attachment: fixed;\">\n    <div class=\"container\">\n      <div class=\"section-title style2 text-center\">\n        <h1 class=\"display-3\" style=\"color: whitesmoke\">MARVEL CINEMATIC UNIVERSE</h1>\n        <p class=\"lead\" style=\"color: whitesmoke\"> Know The Hidden / Latest Gossips Of Marvels. </p>\n      </div>\n      <hr class=\"my-2\">\n      <br>\n      <br>\n      <div class=\"section-title style2 text-center\">\n        <button button type=\"button\" class=\"btn btn-carts\" routerLink=\"/dashboard/blog/listb\">Check Blogs</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<app-user-footer></app-user-footer>\n\n\n<p-dialog header=\"Checkout\" [(visible)]=\"display\" [modal]=\"true\" [responsive]=\"true\" [width]=\"350\" [height]=\"850\" [minWidth]=\"200\"\n  [minY]=\"70\" [maximizable]=\"true\" [baseZIndex]=\"10000\">\n  <div>\n    <h5 class=\"text-center\">Address</h5>\n    <form class=\"address-form\" [formGroup]=\"addressForm\" (ngSubmit)=\"addAddress()\">\n\n      <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <input id=\"float-input\" type=\"text\" class=\"form-control\" formControlName=\"street\" pInputText>\n          <label for=\"float-input\">Adderss</label>\n        </span>\n        <div *ngIf=\"addressForm.controls['street'].touched && !addressForm.controls['street'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n          <span *ngIf=\"addressForm.controls['street'].hasError('required')\">Street required.</span>\n          <span *ngIf=\"addressForm.controls['street'].hasError('maxlength')\">Street Must be less than 50 letters.</span>\n        </div>\n      </div>\n\n      <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <p-dropdown [options]=\"city\" [style]=\"{'width':'100%'}\" formControlName=\"city\"></p-dropdown>\n          <label for=\"float-input\">Select City</label>\n        </span>\n        <div *ngIf=\"addressForm.controls['city'].touched && !addressForm.controls['city'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n          <span *ngIf=\"addressForm.controls['city'].hasError('required')\">City required.</span>\n        </div>\n      </div>\n\n      <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <input id=\"float-input\" type=\"number\" class=\"form-control\" formControlName=\"pincode\" pInputText>\n          <label for=\"float-input\">Pincode</label>\n        </span>\n        <div *ngIf=\"addressForm.controls['pincode'].touched && !addressForm.controls['pincode'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n          <span *ngIf=\"addressForm.controls['pincode'].hasError('required')\">Pincode required.</span>\n        </div>\n      </div>\n\n      <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <input id=\"disabled-input\" type=\"text\" placeholder=\"Maharashtra\" class=\"form-control\" pInputText [disabled]=\"disabled\" />\n        </span>\n      </div>\n\n      <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <input id=\"disabled-input\" type=\"text\" placeholder=\"India\" class=\"form-control\" pInputText [disabled]=\"disabled\" />\n        </span>\n      </div>\n\n      <!-- <h5 class=\"text-center\">Payment Method</h5>\n      <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <input id=\"disabled-input\" type=\"text\" placeholder=\"Cash On Delivery\" class=\"form-control\" pInputText [disabled]=\"disabled\" />\n        </span>\n      </div> -->\n\n      <!-- <div class=\"form-group pt-2\">\n        <span class=\"ui-float-label\">\n          <p-dropdown [options]=\"paymentMethod\" [style]=\"{'width':'100%'}\" formControlName=\"paymentMethod\"></p-dropdown>\n        </span>\n        <div *ngIf=\"addressForm.controls['paymentMethod'].touched && !addressForm.controls['paymentMethod'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n          <span *ngIf=\"addressForm.controls['paymentMethod'].hasError('required')\">PaymentMethod required.</span>\n        </div>\n      </div> -->\n\n      <div class=\"form-check\">\n        <button type=\"submit\" class=\"btn btn-login float-right\" [disabled]=\"!addressForm.valid\">Place Order</button>\n      </div>\n    </form>\n  </div>\n</p-dialog>\n\n<p-dialog header=\"Place Order\" [(visible)]=\"placeOrder\" [modal]=\"true\" [responsive]=\"true\" [width]=\"550\" [minWidth]=\"200\"\n  [minY]=\"70\" [maximizable]=\"false\" [baseZIndex]=\"10000\">\n  <span>Enter Your Credentials Plz</span>\n\n  <form #checkout=\"ngForm\" (ngSubmit)=\"onSubmit(checkout)\" class=\"checkout\">\n    <div class=\"form-row\">\n      <label for=\"card-info\">Card Info</label>\n      <div id=\"card-info\" style=\"width: 500px\" data-zip-code=\"false\" #cardInfo></div>\n\n      <div id=\"card-errors\" role=\"alert\" *ngIf=\"error\">{{ error }}</div>\n    </div>\n\n    <button type=\"submit\">Pay ${{sum}}</button>\n  </form>\n</p-dialog>\n"

/***/ }),

/***/ "./src/app/areas/users/products/cart/cart.component.scss":
/*!***************************************************************!*\
  !*** ./src/app/areas/users/products/cart/cart.component.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-login {\n  background: #4ec67f;\n  color: #fff; }\n\nnav {\n  background: #4ec67f; }\n\n.btn-blogs {\n  background: transparent;\n  border: 2px solid #cecece;\n  border-radius: 12px;\n  color: #fff; }\n\nsection {\n  padding: 60px 0; }\n\nsection .section-title {\n  text-align: center;\n  color: #4ec67f;\n  text-transform: uppercase; }\n\n#footer {\n  background: #4ec67f !important; }\n\n#footer h5 {\n  padding-left: 10px;\n  border-left: 3px solid #eeeeee;\n  color: #ffffff; }\n\n#footer a {\n  color: #ffffff;\n  text-decoration: none !important;\n  background-color: transparent;\n  -webkit-text-decoration-skip: objects; }\n\n#footer ul.social li {\n  padding: 3px 0; }\n\n#footer ul.social li a i {\n  margin-right: 5px;\n  font-size: 25px;\n  transition: .5s all ease; }\n\n#footer ul.social li:hover a i {\n  font-size: 30px;\n  margin-top: -10px; }\n\n#footer ul.social li a,\n#footer ul.quick-links li a {\n  color: #ffffff; }\n\n#footer ul.social li a:hover {\n  color: #eeeeee; }\n\n#footer ul.quick-links li {\n  padding: 3px 0;\n  transition: .5s all ease; }\n\n#footer ul.quick-links li:hover {\n  padding: 3px 0;\n  margin-left: 5px;\n  font-weight: 700; }\n\n#footer ul.quick-links li a i {\n  margin-right: 5px; }\n\n#footer ul.quick-links li:hover a i {\n  font-weight: 700; }\n\n@media (max-width: 767px) {\n  #footer h5 {\n    padding-left: 0;\n    border-left: transparent;\n    padding-bottom: 0px;\n    margin-bottom: 10px; } }\n\n#icons:hover {\n  height: 120px;\n  /* Safari */\n  transition: width 2s; }\n\n.ui-table.borderless thead th,\n.ui-table.borderless tbody,\n.ui-table.borderless tbody tr,\n.ui-table.borderless tbody td {\n  border-style: none; }\n\nform.checkout {\n  max-width: 500px;\n  margin: 2rem auto;\n  text-align: center;\n  border: 2px solid #eee;\n  border-radius: 8px;\n  padding: 1rem 2rem;\n  background: white;\n  font-family: monospace;\n  color: #525252;\n  font-size: 1.1rem; }\n\nform.checkout button {\n  padding: 0.5rem 1rem;\n  color: white;\n  background: coral;\n  border: none;\n  border-radius: 4px;\n  margin-top: 1rem; }\n\nform.checkout button:active {\n  background: #a54c2b; }\n\n.StripeElement {\n  margin: 1rem 0 1rem;\n  background-color: white;\n  padding: 8px 12px;\n  border-radius: 4px;\n  border: 1px solid transparent;\n  box-shadow: 0 1px 3px 0 #e6ebf1;\n  transition: box-shadow 150ms ease; }\n\n.StripeElement--focus {\n  box-shadow: 0 1px 3px 0 #cfd7df; }\n\n.StripeElement--invalid {\n  border-color: #fa755a; }\n\n.StripeElement--webkit-autofill {\n  background-color: #fefde5 !important; }\n"

/***/ }),

/***/ "./src/app/areas/users/products/cart/cart.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/areas/users/products/cart/cart.component.ts ***!
  \*************************************************************/
/*! exports provided: CartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartComponent", function() { return CartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cartServices_list_cart_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/cartServices/list-cart.service */ "./src/app/services/cartServices/list-cart.service.ts");
/* harmony import */ var _services_productServices_list_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/productServices/list-data.service */ "./src/app/services/productServices/list-data.service.ts");
/* harmony import */ var _services_cartServices_edit_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/cartServices/edit-cart.service */ "./src/app/services/cartServices/edit-cart.service.ts");
/* harmony import */ var _services_cartServices_delete_cart_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/cartServices/delete-cart.service */ "./src/app/services/cartServices/delete-cart.service.ts");
/* harmony import */ var _services_cartServices_add_to_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/cartServices/add-to-cart.service */ "./src/app/services/cartServices/add-to-cart.service.ts");
/* harmony import */ var _services_ordersService_add_order_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/ordersService/add-order.service */ "./src/app/services/ordersService/add-order.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_payment_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/payment.service */ "./src/app/services/payment.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var CartComponent = /** @class */ (function () {
    function CartComponent(payment, cd, addOrder, listData, addCart, listCart, edit, deleteData) {
        this.payment = payment;
        this.cd = cd;
        this.addOrder = addOrder;
        this.listData = listData;
        this.addCart = addCart;
        this.listCart = listCart;
        this.edit = edit;
        this.deleteData = deleteData;
        this.display = false;
        this.placeOrder = false;
        this.disabled = true;
        this.cardHandler = this.onChange.bind(this);
        this.city = [
            { label: 'Select City', value: null },
            { label: 'Nagpur', value: 'nagpur' },
            { label: 'Pune', value: 'pune' },
            { label: 'Mumbai', value: 'mumbai' },
        ];
        // this.paymentMethod = [
        //   { label: 'Select Payment', value: null },
        //   { label: 'Cash On Delivery', value: 'cod' },
        //   { label: 'Debit/Credit', value: 'd/c' }
        // ];
    }
    CartComponent.prototype.ngOnInit = function () {
        this.getDetails();
        this.getProducts();
        this.addressForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormGroup"]({
            "street": new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].maxLength(50)]),
            "city": new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required]),
            "pincode": new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required]),
            "state": new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('maharashtra'),
            "country": new _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormControl"]('india')
        });
    };
    CartComponent.prototype.ngAfterViewInit = function () {
        var style = {
            base: {
                lineHeight: '24px',
                fontFamily: 'monospace',
                fontSmoothing: 'antialiased',
                fontSize: '19px',
                '::placeholder': {
                    color: 'purple'
                }
            }
        };
        this.card = elements.create('card', { style: style });
        this.card.mount(this.cardInfo.nativeElement);
        this.card.addEventListener('change', this.cardHandler);
    };
    CartComponent.prototype.ngOnDestroy = function () {
        this.card.removeEventListener('change', this.cardHandler);
        this.card.destroy();
    };
    CartComponent.prototype.onChange = function (_a) {
        var error = _a.error;
        if (error) {
            this.error = error.message;
        }
        else {
            this.error = null;
        }
        this.cd.detectChanges();
    };
    CartComponent.prototype.onSubmit = function (form) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a, token, error;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, stripe.createToken(this.card, {
                            email: this.emailAddress
                        })];
                    case 1:
                        _a = _b.sent(), token = _a.token, error = _a.error;
                        if (error) {
                            console.log('Something is wrong:', error);
                        }
                        else {
                            console.log('Success!', token);
                            this.data = {
                                amount: this.sum,
                                stripeToken: token.id,
                                userId: this.id
                            };
                            console.log('Success!', this.data);
                            this.payment.done(this.data)
                                .subscribe(function (res) {
                                res = res;
                                _this.getDetails();
                                _this.placeOrder = false;
                            });
                            // ...send the token to the your backend to process the charge
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ///////////////////////////////////
    CartComponent.prototype.getDetails = function () {
        var _this = this;
        this.id = localStorage.getItem("id"),
            this.listCart.listData(this.id).subscribe(function (res) {
                _this.carts = res;
                _this.sum = 0;
                for (var i = 0; i < res.length; i++) {
                    _this.sum += res[i].total;
                }
                console.log("Sum:", _this.sum);
            });
    };
    CartComponent.prototype.getProducts = function () {
        var _this = this;
        this.listData.listData().subscribe(function (res) {
            _this.products = res;
        });
    };
    CartComponent.prototype.addToCart = function (productId, productCost) {
        var _this = this;
        this.data = {
            userId: localStorage.getItem("id"),
            productId: productId,
            productCost: productCost,
            quantity: 1,
        };
        console.log("***********", this.data);
        this.addCart.addToCart(this.data)
            .subscribe(function (res) {
            res = res;
            _this.getDetails();
        });
    };
    CartComponent.prototype.updateCart = function (pId, pCost, pQty) {
        var _this = this;
        console.log("aah aah aah aah", pId, pQty);
        var data = {
            userId: localStorage.getItem("id"),
            productId: pId,
            productCost: pCost,
            quantity: pQty,
        };
        console.log("***********", data);
        this.edit.editCart(data)
            .subscribe(function (res) {
            res = res;
            _this.getDetails();
        });
    };
    CartComponent.prototype.deleteCart = function (iD) {
        var _this = this;
        console.log("***********", iD);
        this.deleteData.deleteData(iD).subscribe(function (res) {
            console.log('Updated Cart Data after delete', res);
            _this.getDetails();
        });
    };
    CartComponent.prototype.showDialog = function () {
        this.display = true;
    };
    CartComponent.prototype.addAddress = function () {
        var _this = this;
        this.address = "Country:'" + this.addressForm.value.country +
            "', State:'" + this.addressForm.value.state +
            "', City:'" + this.addressForm.value.city +
            "', Pincode:'" + this.addressForm.value.pincode +
            "', Street:'" + this.addressForm.value.street + "'.";
        this.addressForm.reset();
        this.display = false;
        this.placeOrder = true;
        var data = {
            total: this.sum,
            userId: this.id,
            address: this.address,
        };
        console.log("*************************", data);
        this.addOrder.addToOrders(data)
            .subscribe(function (res) {
            res = res;
            _this.getDetails();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], CartComponent.prototype, "amount", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], CartComponent.prototype, "label", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('payElement'),
        __metadata("design:type", Object)
    ], CartComponent.prototype, "payElement", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('cardInfo'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CartComponent.prototype, "cardInfo", void 0);
    CartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-cart',
            template: __webpack_require__(/*! ./cart.component.html */ "./src/app/areas/users/products/cart/cart.component.html"),
            styles: [__webpack_require__(/*! ./cart.component.scss */ "./src/app/areas/users/products/cart/cart.component.scss")]
        })
        // , AfterViewInit, OnDestroy 
        ,
        __metadata("design:paramtypes", [_services_payment_service__WEBPACK_IMPORTED_MODULE_8__["PaymentService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
            _services_ordersService_add_order_service__WEBPACK_IMPORTED_MODULE_6__["AddOrderService"],
            _services_productServices_list_data_service__WEBPACK_IMPORTED_MODULE_2__["ListDataService"],
            _services_cartServices_add_to_cart_service__WEBPACK_IMPORTED_MODULE_5__["AddToCartService"],
            _services_cartServices_list_cart_service__WEBPACK_IMPORTED_MODULE_1__["ListCartService"],
            _services_cartServices_edit_cart_service__WEBPACK_IMPORTED_MODULE_3__["EditCartService"],
            _services_cartServices_delete_cart_service__WEBPACK_IMPORTED_MODULE_4__["DeleteCartService"]])
    ], CartComponent);
    return CartComponent;
}());



/***/ }),

/***/ "./src/app/areas/users/products/product-listing/product-listing.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/areas/users/products/product-listing/product-listing.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-user-header></app-user-header>\n\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div id=\"demo\" class=\"carousel slide\"  data-interval=3000 data-ride=\"carousel\">\n      <!-- Indicators -->\n      <ul class=\"carousel-indicators\">\n        <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\n        <li data-target=\"#demo\" data-slide-to=\"1\"></li>\n        <!-- <li data-target=\"#demo\" data-slide-to=\"2\"></li> -->\n      </ul>\n      <!-- The slideshow -->\n      <div class=\"carousel-inner\">\n        <div class=\"carousel-item active\">\n          <img src=\"/assets/images/MarvelStudios.jpg\" alt=\"Marvel\" width=\"100%\" height=\"500\">\n        </div>\n        <div class=\"carousel-item\">\n          <img src=\"/assets/images/c1.jpg\" alt=\"Chicago\" width=\"100%\" height=\"500\">\n        </div>\n        <!-- <div class=\"carousel-item\">\n          <img src=\"/assets/images/c3.jpg\" alt=\"New York\" width=\"100%\" height=\"500\">\n        </div> -->\n      </div>\n      <!-- Left and right controls -->\n      <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\n        <span class=\"carousel-control-prev-icon\"></span>\n      </a>\n      <a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\n        <span class=\"carousel-control-next-icon\"></span>\n      </a>\n    </div>\n  </div>\n</div>\n\n\n<div class=\"container\">\n  <div class=\"section-title style2 text-center\" style=\"font-family: 'Source Sans Pro', sans-serif;padding: 10px;\">\n    <br>\n    <h3>NEW ARRIVALS</h3>\n    <br>\n  </div>\n  <div class=\"p-carousel\">\n    <p-carousel [value]=\"products\" numVisible=\"4\" autoplayInterval=\"8500\">\n      <ng-template let-product pTemplate=\"cards\">\n        <p-card title=\"{{product.pname | uppercase}}\" subtitle=\"Rs. {{product.pcost}}\" [style]=\"{width: '100%', height: '520px'}\"\n          styleClass=\"ui-card-shadow\">\n          <img src=\"{{product.imagePath}}\" alt=\"\" width=\"100%\" height=\"200px\" style=\"border-radius: 15%\">\n          <br>\n          <br>\n          <div style=\"height: 50%;\">\n            <p-scrollPanel [style]=\"{width: '100%', height: '73px'}\">\n              <div style=\"padding: 5px;word-wrap:break-word\" [innerHTML]=\"product.description\"></div>\n            </p-scrollPanel>\n          </div>\n          <p-footer>\n            <div class=\"section-title style2 text-center\" style=\"color: black;padding: 5px\">\n              XL/ XXL/ S\n            </div>\n            <div class=\"section-title style2 text-center\">\n              <button button type=\"button\" class=\"btn btn-login\" (click)=addToCart(product._id,product.pcost)>Add to Cart</button>\n            </div>\n          </p-footer>\n        </p-card>\n        <br>\n      </ng-template>\n    </p-carousel>\n    <br>\n    <br>\n  </div>\n</div>\n\n<div style=\"height: 100%\">\n  <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('/assets/images/1.jpg');background-repeat: no-repeat;  background-position: center;background-size: cover;width: 100%;height: 100%;background-attachment: fixed;\">\n    <div class=\"container\">\n      <div class=\"section-title style2 text-center\">\n        <h1 class=\"display-3\" style=\"color: whitesmoke\">MARVEL CINEMATIC UNIVERSE</h1>\n        <p class=\"lead\" style=\"color: whitesmoke\"> Know The Hidden / Latest Gossips Of Marvels. </p>\n      </div>\n      <hr class=\"my-2\">\n      <br>\n      <br>\n      <div class=\"section-title style2 text-center\">\n        <button button type=\"button\" class=\"btn btn-blogs\" routerLink=\"/dashboard/blog/listb\">Check Blogs</button>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"container\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-md-4\" *ngFor=\"let product of products\">\n        <br>\n        <div class=\"container\">\n          <br>\n          <p-card title=\"{{product.pname | uppercase}}\" id=\"prod\" subtitle=\"Rs. {{product.pcost}}\" [style]=\"{width: '100%', height: '520px'}\"\n            styleClass=\"ui-card-shadow\">\n            <img src=\"{{product.imagePath}}\" alt=\"\" width=\"100%\" height=\"200px\" style=\"border-radius: 10%\">\n            <br>\n            <br>\n            <div style=\"height: 100px;\">\n              <p-scrollPanel [style]=\"{width: '100%', height: '85px'}\">\n                <div style=\"padding: 5px;word-wrap:break-word\" [innerHTML]=\"product.description\"></div>\n              </p-scrollPanel>\n            </div>\n            <p-footer>\n              <div class=\"section-title style2 text-center\" style=\"color: black;padding: 5px\">\n                XL/ XXL/ S\n              </div>\n              <div class=\"section-title style2 text-center\">\n                <button button type=\"button\" class=\"btn btn-login\" (click)=addToCart(product._id,product.pcost)>Add to Cart</button>\n              </div>\n            </p-footer>\n          </p-card>\n          <br>\n        </div>\n        <hr>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n<div class=\"container\">\n  <div class=\"row\">\n    <div style=\"height:130px;\">\n      <img src=\"/assets/images/1.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/2.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/3.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/4.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/5.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/6.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/11.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/12.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/13.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n      <img src=\"/assets/images/14.png\" alt=\"Marvel\" id=\"icons\" width=\"10%\" height=\"100px\">\n    </div>\n  </div>\n</div>\n<br>\n\n<div style=\"height: 300px\">\n  <div class=\"jumbotron jumbotron-fluid\" style=\"background-image: url('/assets/images/4.jpg');background-repeat: no-repeat;  background-position: center;background-size: cover;width: 100%;height: 100%;background-attachment: fixed;\">\n    <div class=\"container\">\n      <div class=\"section-title style2 text-center\">\n        <h1 class=\"display-3\" style=\"color: whitesmoke\">TRUSTED SELLERS 500+</h1>\n        <p class=\"lead\" style=\"color: whitesmoke\"> Know The Hidden / Latest Gossips Of Marvels. </p>\n      </div>\n    </div>\n  </div>\n</div>\n\n<app-user-footer></app-user-footer>"

/***/ }),

/***/ "./src/app/areas/users/products/product-listing/product-listing.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/areas/users/products/product-listing/product-listing.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-login {\n  background: #4ec67f;\n  color: #fff; }\n\nnav {\n  background: #4ec67f; }\n\n.btn-blogs {\n  background: transparent;\n  border: 2px solid #cecece;\n  border-radius: 12px;\n  color: #fff; }\n"

/***/ }),

/***/ "./src/app/areas/users/products/product-listing/product-listing.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/areas/users/products/product-listing/product-listing.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ProductListingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListingComponent", function() { return ProductListingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_productServices_list_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/productServices/list-data.service */ "./src/app/services/productServices/list-data.service.ts");
/* harmony import */ var _services_productServices_delete_list_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/productServices/delete-list.service */ "./src/app/services/productServices/delete-list.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_userService_logout_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/userService/logout-user.service */ "./src/app/services/userService/logout-user.service.ts");
/* harmony import */ var _services_cartServices_add_to_cart_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/cartServices/add-to-cart.service */ "./src/app/services/cartServices/add-to-cart.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProductListingComponent = /** @class */ (function () {
    function ProductListingComponent(listData, deleteList, logOut, root, addCart) {
        this.listData = listData;
        this.deleteList = deleteList;
        this.logOut = logOut;
        this.root = root;
        this.addCart = addCart;
    }
    ProductListingComponent.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ProductListingComponent.prototype.getDetails = function () {
        var _this = this;
        this.listData.listData().subscribe(function (res) {
            _this.products = res;
        });
    };
    ProductListingComponent.prototype.delete = function (id) {
        this.deleteList.deleteData(id).subscribe(function (res) {
            console.log("deleted");
        });
        this.getDetails();
    };
    ProductListingComponent.prototype.logout = function () {
        this.logOut.out(this.id)
            .subscribe(function (res) {
            console.log("LogedOut", res);
        });
        localStorage.clear();
        this.root.navigate(['/']);
    };
    ProductListingComponent.prototype.addToCart = function (productId, productCost) {
        this.data = {
            userId: localStorage.getItem("id"),
            productId: productId,
            productCost: productCost,
            quantity: 1,
        };
        console.log("***********", this.data);
        this.addCart.addToCart(this.data)
            .subscribe(function (res) {
            res = res;
        });
    };
    ProductListingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-listing',
            template: __webpack_require__(/*! ./product-listing.component.html */ "./src/app/areas/users/products/product-listing/product-listing.component.html"),
            styles: [__webpack_require__(/*! ./product-listing.component.scss */ "./src/app/areas/users/products/product-listing/product-listing.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_productServices_list_data_service__WEBPACK_IMPORTED_MODULE_1__["ListDataService"],
            _services_productServices_delete_list_service__WEBPACK_IMPORTED_MODULE_2__["DeleteListService"],
            _services_userService_logout_user_service__WEBPACK_IMPORTED_MODULE_4__["LogoutUserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _services_cartServices_add_to_cart_service__WEBPACK_IMPORTED_MODULE_5__["AddToCartService"]])
    ], ProductListingComponent);
    return ProductListingComponent;
}());



/***/ }),

/***/ "./src/app/areas/users/products/products-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/areas/users/products/products-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: ProductsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsRoutingModule", function() { return ProductsRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-listing/product-listing.component */ "./src/app/areas/users/products/product-listing/product-listing.component.ts");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cart/cart.component */ "./src/app/areas/users/products/cart/cart.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: "listp", component: _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_3__["ProductListingComponent"] },
    { path: "cart", component: _cart_cart_component__WEBPACK_IMPORTED_MODULE_4__["CartComponent"] },
];
var ProductsRoutingModule = /** @class */ (function () {
    function ProductsRoutingModule() {
    }
    ProductsRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: []
        })
    ], ProductsRoutingModule);
    return ProductsRoutingModule;
}());



/***/ }),

/***/ "./src/app/areas/users/products/products.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/areas/users/products/products.module.ts ***!
  \*********************************************************/
/*! exports provided: ProductsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsModule", function() { return ProductsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _products_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! .//products-routing.module */ "./src/app/areas/users/products/products-routing.module.ts");
/* harmony import */ var _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-listing/product-listing.component */ "./src/app/areas/users/products/product-listing/product-listing.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var primeng_carousel__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/carousel */ "./node_modules/primeng/carousel.js");
/* harmony import */ var primeng_carousel__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(primeng_carousel__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/card */ "./node_modules/primeng/card.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(primeng_card__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/scrollpanel */ "./node_modules/primeng/scrollpanel.js");
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/spinner */ "./node_modules/primeng/spinner.js");
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_spinner__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./cart/cart.component */ "./src/app/areas/users/products/cart/cart.component.ts");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/dialog.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(primeng_dialog__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/inputtext */ "./node_modules/primeng/inputtext.js");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _users_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../users.module */ "./src/app/areas/users/users.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var ProductsModule = /** @class */ (function () {
    function ProductsModule() {
    }
    ProductsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _products_routing_module__WEBPACK_IMPORTED_MODULE_2__["ProductsRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_5__["TableModule"],
                primeng_carousel__WEBPACK_IMPORTED_MODULE_6__["CarouselModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_13__["InputTextModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_7__["CardModule"],
                primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_8__["ScrollPanelModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_9__["SpinnerModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_11__["DialogModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__["DropdownModule"],
                _users_module__WEBPACK_IMPORTED_MODULE_14__["UsersModule"],
            ],
            declarations: [_product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_3__["ProductListingComponent"], _cart_cart_component__WEBPACK_IMPORTED_MODULE_10__["CartComponent"],
            ]
        })
    ], ProductsModule);
    return ProductsModule;
}());



/***/ }),

/***/ "./src/app/services/cartServices/add-to-cart.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/cartServices/add-to-cart.service.ts ***!
  \**************************************************************/
/*! exports provided: AddToCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddToCartService", function() { return AddToCartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddToCartService = /** @class */ (function () {
    function AddToCartService(http) {
        this.http = http;
    }
    AddToCartService.prototype.addToCart = function (data) {
        console.log("add cart service", data);
        return this.http.post('http://localhost:10010/Cart/', data);
    };
    AddToCartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AddToCartService);
    return AddToCartService;
}());



/***/ }),

/***/ "./src/app/services/cartServices/delete-cart.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/cartServices/delete-cart.service.ts ***!
  \**************************************************************/
/*! exports provided: DeleteCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteCartService", function() { return DeleteCartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DeleteCartService = /** @class */ (function () {
    function DeleteCartService(http) {
        this.http = http;
    }
    DeleteCartService.prototype.deleteData = function (id) {
        console.log("Deleted", id);
        return this.http.delete('http://localhost:10010/Cart?id=' + id);
    };
    DeleteCartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DeleteCartService);
    return DeleteCartService;
}());



/***/ }),

/***/ "./src/app/services/cartServices/edit-cart.service.ts":
/*!************************************************************!*\
  !*** ./src/app/services/cartServices/edit-cart.service.ts ***!
  \************************************************************/
/*! exports provided: EditCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditCartService", function() { return EditCartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EditCartService = /** @class */ (function () {
    function EditCartService(http) {
        this.http = http;
    }
    EditCartService.prototype.editCart = function (data) {
        console.log("add cart service", data);
        return this.http.put('http://localhost:10010/Cart/', data);
    };
    EditCartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EditCartService);
    return EditCartService;
}());



/***/ }),

/***/ "./src/app/services/cartServices/list-cart.service.ts":
/*!************************************************************!*\
  !*** ./src/app/services/cartServices/list-cart.service.ts ***!
  \************************************************************/
/*! exports provided: ListCartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListCartService", function() { return ListCartService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListCartService = /** @class */ (function () {
    function ListCartService(http) {
        this.http = http;
    }
    ListCartService.prototype.listData = function (userId) {
        return this.http.get('http://localhost:10010/Cart?userId=' + userId);
    };
    ListCartService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ListCartService);
    return ListCartService;
}());



/***/ }),

/***/ "./src/app/services/ordersService/add-order.service.ts":
/*!*************************************************************!*\
  !*** ./src/app/services/ordersService/add-order.service.ts ***!
  \*************************************************************/
/*! exports provided: AddOrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddOrderService", function() { return AddOrderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddOrderService = /** @class */ (function () {
    function AddOrderService(http) {
        this.http = http;
    }
    AddOrderService.prototype.addToOrders = function (data) {
        console.log("add data service", data);
        return this.http.post('http://localhost:10010/Orders/', data);
    };
    AddOrderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AddOrderService);
    return AddOrderService;
}());



/***/ }),

/***/ "./src/app/services/payment.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/payment.service.ts ***!
  \*********************************************/
/*! exports provided: PaymentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentService", function() { return PaymentService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaymentService = /** @class */ (function () {
    function PaymentService(http) {
        this.http = http;
    }
    PaymentService.prototype.done = function (data) {
        console.log("Added", data);
        return this.http.post('http://localhost:10010/Payment', data);
    };
    PaymentService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], PaymentService);
    return PaymentService;
}());



/***/ })

}]);
//# sourceMappingURL=products-products-module.js.map