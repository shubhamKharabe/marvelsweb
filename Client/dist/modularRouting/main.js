(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./areas/admin/admin.module": [
		"./src/app/areas/admin/admin.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"areas-admin-admin-module~blog-management-blog-management-module~product-management-product-managemen~f3e522a7",
		"common",
		"areas-admin-admin-module"
	],
	"./areas/globalPages/service-pages/service-pages.module": [
		"./src/app/areas/globalPages/service-pages/service-pages.module.ts",
		"areas-globalPages-service-pages-service-pages-module"
	],
	"./areas/users/users.module": [
		"./src/app/areas/users/users.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"areas-users-users-module~blogs-blogs-module~products-products-module",
		"common"
	],
	"./blog-management/blog-management.module": [
		"./src/app/areas/admin/blog-management/blog-management.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"blog-management-blog-management-module~blogs-blogs-module~library-auth-auth-module~product-managemen~868076bd",
		"blog-management-blog-management-module~library-auth-auth-module~product-management-product-managemen~a46cb3e6",
		"blog-management-blog-management-module~product-management-product-management-module~products-product~a8c3b280",
		"areas-admin-admin-module~blog-management-blog-management-module~product-management-product-managemen~f3e522a7",
		"blog-management-blog-management-module~blogs-blogs-module~product-management-product-management-modu~3fba6866",
		"blog-management-blog-management-module~library-auth-auth-module~user-management-user-management-modu~a367210b",
		"blog-management-blog-management-module~blogs-blogs-module",
		"common",
		"blog-management-blog-management-module"
	],
	"./blogs/blogs.module": [
		"./src/app/areas/users/blogs/blogs.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"blog-management-blog-management-module~blogs-blogs-module~library-auth-auth-module~product-managemen~868076bd",
		"blog-management-blog-management-module~blogs-blogs-module~product-management-product-management-modu~3fba6866",
		"areas-users-users-module~blogs-blogs-module~products-products-module",
		"blog-management-blog-management-module~blogs-blogs-module",
		"common",
		"blogs-blogs-module"
	],
	"./library/auth/auth.module": [
		"./src/app/library/auth/auth.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"blog-management-blog-management-module~blogs-blogs-module~library-auth-auth-module~product-managemen~868076bd",
		"blog-management-blog-management-module~library-auth-auth-module~product-management-product-managemen~a46cb3e6",
		"blog-management-blog-management-module~library-auth-auth-module~user-management-user-management-modu~a367210b",
		"library-auth-auth-module~products-products-module",
		"common",
		"library-auth-auth-module"
	],
	"./product-management/product-management.module": [
		"./src/app/areas/admin/product-management/product-management.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"blog-management-blog-management-module~blogs-blogs-module~library-auth-auth-module~product-managemen~868076bd",
		"blog-management-blog-management-module~library-auth-auth-module~product-management-product-managemen~a46cb3e6",
		"blog-management-blog-management-module~product-management-product-management-module~products-product~a8c3b280",
		"areas-admin-admin-module~blog-management-blog-management-module~product-management-product-managemen~f3e522a7",
		"blog-management-blog-management-module~blogs-blogs-module~product-management-product-management-modu~3fba6866",
		"common",
		"product-management-product-management-module"
	],
	"./products/products.module": [
		"./src/app/areas/users/products/products.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"blog-management-blog-management-module~blogs-blogs-module~library-auth-auth-module~product-managemen~868076bd",
		"blog-management-blog-management-module~library-auth-auth-module~product-management-product-managemen~a46cb3e6",
		"blog-management-blog-management-module~product-management-product-management-module~products-product~a8c3b280",
		"blog-management-blog-management-module~blogs-blogs-module~product-management-product-management-modu~3fba6866",
		"areas-users-users-module~blogs-blogs-module~products-products-module",
		"library-auth-auth-module~products-products-module",
		"common",
		"products-products-module"
	],
	"./user-management/user-management.module": [
		"./src/app/areas/admin/user-management/user-management.module.ts",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~46b34996",
		"areas-admin-admin-module~areas-users-users-module~blog-management-blog-management-module~blogs-blogs~881bf767",
		"blog-management-blog-management-module~blogs-blogs-module~library-auth-auth-module~product-managemen~868076bd",
		"blog-management-blog-management-module~library-auth-auth-module~product-management-product-managemen~a46cb3e6",
		"blog-management-blog-management-module~product-management-product-management-module~products-product~a8c3b280",
		"areas-admin-admin-module~blog-management-blog-management-module~product-management-product-managemen~f3e522a7",
		"blog-management-blog-management-module~library-auth-auth-module~user-management-user-management-modu~a367210b",
		"common",
		"user-management-user-management-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error('Cannot find module "' + req + '".');
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var module = __webpack_require__(ids[0]);
		return module;
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _library_shared_components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./library/shared/components/home/home.component */ "./src/app/library/shared/components/home/home.component.ts");
/* harmony import */ var _services_authguard_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/authguard.service */ "./src/app/services/authguard.service.ts");
/* harmony import */ var _services_loged_out_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./services/loged-out.service */ "./src/app/services/loged-out.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: "", component: _library_shared_components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: "login", loadChildren: "./library/auth/auth.module#AuthModule", canActivate: [_services_loged_out_service__WEBPACK_IMPORTED_MODULE_5__["LogedOutService"]] },
    { path: "dashboard", loadChildren: "./areas/users/users.module#UsersModule" },
    { path: "adminDashboard", loadChildren: "./areas/admin/admin.module#AdminModule", canActivate: [_services_authguard_service__WEBPACK_IMPORTED_MODULE_4__["AuthguardService"]] },
    { path: "servicePages", loadChildren: "./areas/globalPages/service-pages/service-pages.module#ServicePagesModule" },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! .//app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _library_shared_components_home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./library/shared/components/home/home.component */ "./src/app/library/shared/components/home/home.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/index.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _services_interceptors_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/interceptors.service */ "./src/app/services/interceptors.service.ts");
/* harmony import */ var _services_authguard_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/authguard.service */ "./src/app/services/authguard.service.ts");
/* harmony import */ var _services_loged_out_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/loged-out.service */ "./src/app/services/loged-out.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _library_shared_components_home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_7__["TooltipModule"].forRoot(),
            ],
            providers: [_services_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"], _services_authguard_service__WEBPACK_IMPORTED_MODULE_10__["AuthguardService"], _services_loged_out_service__WEBPACK_IMPORTED_MODULE_11__["LogedOutService"],
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"], useClass: _services_interceptors_service__WEBPACK_IMPORTED_MODULE_9__["InterceptorsService"], multi: true }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/library/shared/components/home/home.component.html":
/*!********************************************************************!*\
  !*** ./src/app/library/shared/components/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 style=\"background: #DE6262; color:#fff; font-weight:600;margin: 0px;padding-left: 5px\"> Marvels </h1>\n<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">\n        <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\n        <a class=\"navbar-brand\" routerLink=\"/\">Home</a>\n        <ul class=\"navbar-nav\">\n            <li class=\"nav-item\">\n                <a class=\"navbar-brand\" routerLink=\"/servicePages/faq\">Faq</a>\n            </li>\n            <li class=\"nav-item\">\n                <a class=\"navbar-brand\" routerLink=\"/servicePages/privacyPolicies\">Privacy Policies</a>\n            </li>\n        </ul>\n    </div>\n    <div class=\"btn-group\" dropdown>\n        <a class=\"navbar-brand\" id=\"login\" routerLink=\"/login\">{{opt}}</a>\n    </div>\n</nav>\n<div class=\"banner-sec\">\n    <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">\n        <ol class=\"carousel-indicators\">\n            <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>\n        </ol>\n        <div class=\"carousel-inner\" role=\"listbox\">\n            <div class=\"carousel-item active\">\n                <img class=\"d-block img-fluid\" src=\"../../assets/images/1.jpg\" alt=\"First slide\">\n                <div class=\"carousel-caption d-none d-md-block\">\n                    <div class=\"banner-text\">\n                        <h2>This is Heaven</h2>\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore\n                            et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n<div class=\"container\" style=\"padding: 40px 20px\">\n    <div class=\"row\">\n        <div class=\"col-md-4 login-sec\">\n            <img class=\"d-block img-fluid\" src=\"../../assets/images/l3.png\" width=\"150px\" height=\"150px\" alt=\"First slide\">\n            <h2>This is Heaven</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n        </div>\n        <div class=\"col-md-4 login-sec\">\n            <img class=\"d-block img-fluid\" src=\"../../assets/images/l3.png\" width=\"150px\" height=\"150px\" alt=\"First slide\">\n            <h2>This is Heaven</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n        </div>\n        <div class=\"col-md-4 login-sec\">\n            <img class=\"d-block img-fluid\" src=\"../../assets/images/l3.png\" width=\"150px\" height=\"150px\" alt=\"First slide\">\n            <h2>This is Heaven</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n        </div>\n    </div>\n</div>\n<footer>\n    <div class=\"container-fluid\">\n        <h2>Contacts:</h2>\n        <div class=\"row\" style=\"padding: 20px;\">\n            <div class=\"col-md-4\">\n                <dt>Address:</dt>\n                <dd>24, Sita Nagar, Nagpur - 440025</dd>\n            </div>\n            <div class=\"col-md-4\">\n                <dt>Email:</dt>\n                <dd>\n                    <a href=\"#\">commmando@gmail.com</a>\n                </dd>\n            </div>\n            <div class=\"col-md-4\">\n                <dt>Phones:</dt>\n                <dd>\n                    <a href=\"#\">+91 7568543012</a>\n                    <span> | </span>\n                    <a href=\"#\">+91 9571195353</a>\n                </dd>\n            </div>\n        </div>\n        <h2 style=\"font-size: 15px;padding-left: 40%;\">©-2018 Commandos.  All Rights Reserved.</h2>\n    </div>\n</footer>"

/***/ }),

/***/ "./src/app/library/shared/components/home/home.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/library/shared/components/home/home.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-sec h2 {\n  margin-bottom: 30px;\n  font-weight: 800;\n  font-size: 30px;\n  color: #DE6262; }\n\n.btn-login {\n  background: #DE6262;\n  color: #fff;\n  font-weight: 600; }\n\nfooter {\n  padding: 5px;\n  background: #212529; }\n\nh2, h5, dt {\n  color: #f7f7f7; }\n\na, dd {\n  color: #DE6262;\n  font-size: 15px; }\n\nnav {\n  padding: 10px; }\n\n.navbar-brand {\n  font-size: 17px;\n  padding: 0px  10px; }\n\n#login {\n  font-size: 19px;\n  color: #DE6262;\n  padding: 0px  13px; }\n"

/***/ }),

/***/ "./src/app/library/shared/components/home/home.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/library/shared/components/home/home.component.ts ***!
  \******************************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.opt = localStorage.uname || "Login";
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/library/shared/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/library/shared/components/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/services/authguard.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/authguard.service.ts ***!
  \***********************************************/
/*! exports provided: AuthguardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthguardService", function() { return AuthguardService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.service */ "./src/app/services/login.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthguardService = /** @class */ (function () {
    function AuthguardService(log, router) {
        this.log = log;
        this.router = router;
    }
    AuthguardService.prototype.canActivate = function (next, state) {
        if (this.log.isLogedAsAdmin()) {
            return true;
        }
        else if (this.log.isLoged()) {
            this.router.navigate(["/dashboard"]);
            return false;
        }
        else {
            this.router.navigate([""]);
            return false;
        }
    };
    AuthguardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthguardService);
    return AuthguardService;
}());



/***/ }),

/***/ "./src/app/services/interceptors.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/interceptors.service.ts ***!
  \**************************************************/
/*! exports provided: InterceptorsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptorsService", function() { return InterceptorsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var InterceptorsService = /** @class */ (function () {
    function InterceptorsService() {
    }
    InterceptorsService.prototype.intercept = function (req, next) {
        var token = localStorage.getItem(token);
        var authRequest = req.clone({
            headers: req.headers.set("Auth", "Bearer" + token)
        });
        return next.handle(authRequest);
    };
    InterceptorsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], InterceptorsService);
    return InterceptorsService;
}());



/***/ }),

/***/ "./src/app/services/loged-out.service.ts":
/*!***********************************************!*\
  !*** ./src/app/services/loged-out.service.ts ***!
  \***********************************************/
/*! exports provided: LogedOutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogedOutService", function() { return LogedOutService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login.service */ "./src/app/services/login.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LogedOutService = /** @class */ (function () {
    function LogedOutService(log, router) {
        this.log = log;
        this.router = router;
    }
    LogedOutService.prototype.canActivate = function (next, state) {
        if (this.log.isLoged()) {
            if (localStorage.role == "admin") {
                this.router.navigate(["/adminDashboard"]);
            }
            else {
                this.router.navigate(["/dashboard"]);
            }
            return false;
        }
        else {
            return true;
        }
    };
    LogedOutService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LogedOutService);
    return LogedOutService;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.login = function (data) {
        console.log("API Hitted");
        return this.http.post('http://localhost:10010/login', data);
    };
    LoginService.prototype.isLoged = function () {
        if ((localStorage.getItem("token")) !== null)
            return true;
    };
    LoginService.prototype.isLogedAsAdmin = function () {
        if ((localStorage.getItem("token")) !== null && (localStorage.getItem("role")) == "admin")
            return true;
    };
    LoginService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/shubhamkharabe/Downloads/the_commando/Angular/modularRouting/beta/Client/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map