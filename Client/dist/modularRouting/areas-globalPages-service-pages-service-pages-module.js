(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["areas-globalPages-service-pages-service-pages-module"],{

/***/ "./src/app/areas/globalPages/service-pages/faq/faq.component.html":
/*!************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/faq/faq.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 style=\"background: #DE6262; color:#fff; font-weight:600;margin: 0px;padding-left: 5px\">\n    Marvels</h1>\n<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\" style=\"padding: 10px 20px\">\n    <a class=\"navbar-brand\" routerLink=\"/login\" style=\"font-size: 20px;margin-right: 3%\">Login</a>\n    <a class=\"navbar-brand\" routerLink=\"/servicePages/faq\">Faq</a>\n    <a class=\"navbar-brand\" routerLink=\"/servicePages/privacyPolicies\">Privacy Policies</a>\n</nav>\n<div class=\"container\">\n    <h3>\n        FAQ Works!\n    </h3><br><br>\n    <h4>This is a paragraph ?</h4>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br><br>\n    <h4>This is a paragraph ?</h4>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br><br>\n    <h4>This is a paragraph ?</h4>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br><br>\n    <h4>This is a paragraph ?</h4>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br><br>\n    <h4>This is a paragraph ?</h4>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br><br>\n    <h4>This is a paragraph ?</h4>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br><br>\n</div>\n<footer>\n    <div class=\"container-fluid\">\n        <h2>Contacts:</h2>\n        <div class=\"row\" style=\"padding: 20px;\">\n            <div class=\"col-md-4\">\n                <dt>Address:</dt>\n                <dd>24, Sita Nagar, Nagpur - 440025</dd>\n            </div>\n            <div class=\"col-md-4\">\n                <dt>Email:</dt>\n                <dd>\n                    <a href=\"#\">commmando@gmail.com</a>\n                </dd>\n            </div>\n            <div class=\"col-md-4\">\n                <dt>Phones:</dt>\n                <dd>\n                    <a href=\"#\">+91 7568543012</a>\n                    <span> | </span>\n                    <a href=\"#\">+91 9571195353</a>\n                </dd>\n            </div>\n        </div>\n        <h2 style=\"font-size: 15px;padding-left: 40%;\">©-2018 Commandos.  All Rights Reserved.</h2>\n    </div>\n</footer>"

/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/faq/faq.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/faq/faq.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li {\n  padding: 0px 20px; }\n\n.container-fluid {\n  padding: 5px;\n  background: #212529;\n  width: 100%; }\n\nh2, h5, dt, dd {\n  color: #f7f7f7; }\n\na {\n  color: #DE6262;\n  font-size: 15px; }\n"

/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/faq/faq.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/faq/faq.component.ts ***!
  \**********************************************************************/
/*! exports provided: FaqComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FaqComponent", function() { return FaqComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FaqComponent = /** @class */ (function () {
    function FaqComponent() {
    }
    FaqComponent.prototype.ngOnInit = function () {
    };
    FaqComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-faq',
            template: __webpack_require__(/*! ./faq.component.html */ "./src/app/areas/globalPages/service-pages/faq/faq.component.html"),
            styles: [__webpack_require__(/*! ./faq.component.scss */ "./src/app/areas/globalPages/service-pages/faq/faq.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FaqComponent);
    return FaqComponent;
}());



/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 style=\"background: #DE6262; color:#fff; font-weight:600;margin: 0px;padding-left: 5px\">\n  Marvels</h1>\n<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\" style=\"padding: 10px 20px\">\n  <a class=\"navbar-brand\" routerLink=\"/login\" style=\"font-size: 20px;margin-right: 3%\">Login</a>\n  <a class=\"navbar-brand\" routerLink=\"/servicePages/faq\">Faq</a>\n  <a class=\"navbar-brand\" routerLink=\"/servicePages/privacyPolicies\">Privacy Policies</a>\n</nav><div class=\"container\">\n    <h3>\n        Privacy-Policyy Works!\n    </h3><br>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.<br>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br>\n    \n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p>\n    <p>This paragraph contains a lot of spaces in the source code, but the browser ignores it.</p><br>\n</div>\n<footer>\n  <div class=\"container-fluid\">\n      <h2>Contacts:</h2>\n      <div class=\"row\" style=\"padding: 20px;\">\n          <div class=\"col-md-4\">\n              <dt>Address:</dt>\n              <dd>24, Sita Nagar, Nagpur - 440025</dd>\n          </div>\n          <div class=\"col-md-4\">\n              <dt>Email:</dt>\n              <dd>\n                  <a href=\"#\">commmando@gmail.com</a>\n              </dd>\n          </div>\n          <div class=\"col-md-4\">\n              <dt>Phones:</dt>\n              <dd>\n                  <a href=\"#\">+91 7568543012</a>\n                  <span> | </span>\n                  <a href=\"#\">+91 9571195353</a>\n              </dd>\n          </div>\n      </div>\n      <h2 style=\"font-size: 15px;padding-left: 40%;\">©-2018 Commandos.  All Rights Reserved.</h2>\n  </div>\n</footer>\n"

/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "li {\n  padding: 0px 20px; }\n\n.container-fluid {\n  padding: 5px;\n  background: #212529;\n  width: 100%; }\n\nh2, h5, dt, dd {\n  color: #f7f7f7; }\n\na {\n  color: #DE6262;\n  font-size: 15px; }\n"

/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.ts ***!
  \********************************************************************************************/
/*! exports provided: PrivacyPolicyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivacyPolicyComponent", function() { return PrivacyPolicyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyPolicyComponent = /** @class */ (function () {
    function PrivacyPolicyComponent() {
    }
    PrivacyPolicyComponent.prototype.ngOnInit = function () {
    };
    PrivacyPolicyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-privacy-policy',
            template: __webpack_require__(/*! ./privacy-policy.component.html */ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.html"),
            styles: [__webpack_require__(/*! ./privacy-policy.component.scss */ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PrivacyPolicyComponent);
    return PrivacyPolicyComponent;
}());



/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/service-pages-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/service-pages-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: ServicePagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicePagesRoutingModule", function() { return ServicePagesRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/areas/globalPages/service-pages/faq/faq.component.ts");
/* harmony import */ var _privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./privacy-policy/privacy-policy.component */ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: "faq", component: _faq_faq_component__WEBPACK_IMPORTED_MODULE_3__["FaqComponent"] },
    { path: "privacyPolicies", component: _privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_4__["PrivacyPolicyComponent"] },
];
var ServicePagesRoutingModule = /** @class */ (function () {
    function ServicePagesRoutingModule() {
    }
    ServicePagesRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: []
        })
    ], ServicePagesRoutingModule);
    return ServicePagesRoutingModule;
}());



/***/ }),

/***/ "./src/app/areas/globalPages/service-pages/service-pages.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/areas/globalPages/service-pages/service-pages.module.ts ***!
  \*************************************************************************/
/*! exports provided: ServicePagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServicePagesModule", function() { return ServicePagesModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _faq_faq_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./faq/faq.component */ "./src/app/areas/globalPages/service-pages/faq/faq.component.ts");
/* harmony import */ var _privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./privacy-policy/privacy-policy.component */ "./src/app/areas/globalPages/service-pages/privacy-policy/privacy-policy.component.ts");
/* harmony import */ var _service_pages_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! .//service-pages-routing.module */ "./src/app/areas/globalPages/service-pages/service-pages-routing.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ServicePagesModule = /** @class */ (function () {
    function ServicePagesModule() {
    }
    ServicePagesModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _service_pages_routing_module__WEBPACK_IMPORTED_MODULE_4__["ServicePagesRoutingModule"]
            ],
            declarations: [_faq_faq_component__WEBPACK_IMPORTED_MODULE_2__["FaqComponent"], _privacy_policy_privacy_policy_component__WEBPACK_IMPORTED_MODULE_3__["PrivacyPolicyComponent"]]
        })
    ], ServicePagesModule);
    return ServicePagesModule;
}());



/***/ })

}]);
//# sourceMappingURL=areas-globalPages-service-pages-service-pages-module.js.map