(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["product-management-product-management-module"],{

/***/ "./src/app/areas/admin/product-management/add-product/add-product.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/add-product/add-product.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-admin-header></app-admin-header>\n\n<section>\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-md-8 banner-sec\">\n                <div id=\"carouselExampleIndicators\" class=\"carousel slide\" data-ride=\"carousel\">\n                    <ol class=\"carousel-indicators\">\n                        <li data-target=\"#carouselExampleIndicators\" data-slide-to=\"0\" class=\"active\"></li>\n                    </ol>\n                    <div class=\"carousel-inner\" role=\"listbox\">\n                        <div class=\"carousel-item active\">\n                            <img class=\"d-block img-fluid\" src=\"/assets/images/6.jpg\" alt=\"First slide\">\n                            <div class=\"carousel-caption d-none d-md-block\">\n                                <div class=\"banner-text\">\n                                    <h2>This is Heaven</h2>\n                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt\n                                        ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                    <h2>This is Heaven</h2>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et\n                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n                </div>\n            </div>\n            <div class=\"col-md-4 login-sec\">\n                <h2 class=\"text-center\">Add Products</h2>\n                <form class=\"login-form\" [formGroup]=\"addProductForm\" (ngSubmit)=\"onSubmit()\">\n                    <div class=\"form-group pt-2\">\n                        <span class=\"ui-float-label\">\n                            <input id=\"float-input\" type=\"text\" size=\"30\" class=\"form-control\" formControlName=\"pname\" pInputText>\n                            <label for=\"float-input\">Product Name</label>\n                        </span>\n                        <div *ngIf=\"addProductForm.controls['pname'].touched && !addProductForm.controls['pname'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n                            <span *ngIf=\"addProductForm.controls['pname'].hasError('required')\">Product Name required.</span>\n                            <span *ngIf=\"addProductForm.controls['pname'].hasError('maxlength')\">Product Name Must be less than 20 words.</span>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group pt-2\">\n                        <span class=\"ui-float-label\">\n                            <input id=\"float-input\" type=\"number\" size=\"30\" class=\"form-control\" formControlName=\"pcost\" pInputText>\n                            <label for=\"float-input\">Product Cost</label>\n                        </span>\n                        <div *ngIf=\"addProductForm.controls['pcost'].touched && !addProductForm.controls['pcost'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n                            <span *ngIf=\"addProductForm.controls['pcost'].hasError('required')\">Product Cost required.</span>\n                            <span *ngIf=\"addProductForm.controls['pcost'].hasError('maxlength')\">Product Cost Must be less than 5 digits.</span>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group pt-2\">\n                        <span class=\"ui-float-label\">\n                            <p-editor formControlName=\"description\" [style]=\"{'height':'120px'}\"></p-editor>\n                        </span>\n                        <div *ngIf=\"addProductForm.controls['description'].touched && !addProductForm.controls['description'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n                            <span *ngIf=\"addProductForm.controls['description'].hasError('required')\">description required.</span>\n                            <span *ngIf=\"addProductForm.controls['description'].hasError('minlength')\">description Must be more than 50 words.</span>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group pt-2\">\n                        <span class=\"ui-float-label\">\n                            <p-dropdown [options]=\"status\" [style]=\"{'width':'100%'}\" formControlName=\"status\"></p-dropdown>\n                            <label for=\"float-input\">Select Status</label>\n                        </span>\n                        <div *ngIf=\"addProductForm.controls['status'].touched && !addProductForm.controls['status'].valid\" class=\"cross-validation-error-message alert alert-danger\">\n                            <span *ngIf=\"addProductForm.controls['status'].hasError('required')\">Status required.</span>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <label for=\"imagePath\" class=\"text-uppercase\">Upload Image</label>\n                        <input type=\"file\" id=\"imagePath\" (change)=\"onUpload($event)\" #fileInput>\n                    </div>\n\n                    <div class=\"form-check\">\n                        <button type=\"submit\" class=\"btn btn-login float-right\" [disabled]=\"!addProductForm.valid\">Submit</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>"

/***/ }),

/***/ "./src/app/areas/admin/product-management/add-product/add-product.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/add-product/add-product.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".carousel-inner {\n  border-radius: 0 10px 10px 0; }\n\n.carousel-caption {\n  text-align: left;\n  left: 5%; }\n\n.btn-login {\n  background: #DE6262;\n  color: #fff;\n  font-weight: 600; }\n\nsection {\n  padding: 50px; }\n"

/***/ }),

/***/ "./src/app/areas/admin/product-management/add-product/add-product.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/add-product/add-product.component.ts ***!
  \*************************************************************************************/
/*! exports provided: AddProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddProductComponent", function() { return AddProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_productServices_add_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/productServices/add-data.service */ "./src/app/services/productServices/add-data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddProductComponent = /** @class */ (function () {
    function AddProductComponent(addData, formBuild, root) {
        this.addData = addData;
        this.formBuild = formBuild;
        this.root = root;
        this.status = [
            { label: 'Select Status', value: null },
            { label: 'Out-Of-Stock', value: 'out-of-stock' },
            { label: 'In-Stock', value: 'in-stock' },
        ];
    }
    AddProductComponent.prototype.ngOnInit = function () {
        this.addProductForm = this.formBuild.group({
            pname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(20)]),
            imagePath: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            pcost: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].maxLength(5)]),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(50)]),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
        });
    };
    AddProductComponent.prototype.onUpload = function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.addProductForm.get('imagePath').setValue(file);
            console.log(file);
        }
    };
    AddProductComponent.prototype.SavaProduct = function () {
        var input = new FormData();
        input.append('pname', this.addProductForm.get('pname').value);
        input.append('imagePath', this.addProductForm.get('imagePath').value);
        input.append('pcost', this.addProductForm.get('pcost').value);
        input.append('description', this.addProductForm.get('description').value);
        input.append('status', this.addProductForm.get('status').value);
        console.log(input);
        return input;
    };
    AddProductComponent.prototype.onSubmit = function () {
        var formModel = this.SavaProduct();
        this.addData.addData(formModel)
            .subscribe(function (res) {
            res = res.data;
        });
        this.root.navigate(['/adminDashboard/products/listp']);
    };
    AddProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-product',
            template: __webpack_require__(/*! ./add-product.component.html */ "./src/app/areas/admin/product-management/add-product/add-product.component.html"),
            styles: [__webpack_require__(/*! ./add-product.component.scss */ "./src/app/areas/admin/product-management/add-product/add-product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_productServices_add_data_service__WEBPACK_IMPORTED_MODULE_2__["AddDataService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AddProductComponent);
    return AddProductComponent;
}());



/***/ }),

/***/ "./src/app/areas/admin/product-management/edit-product/edit-product.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/edit-product/edit-product.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-admin-header></app-admin-header>\n\n<div class=\"container\">\n  <div class=\"container\">\n    <br>\n    <br>\n    <h3>Edit Product:</h3>\n    <br>\n  </div>\n  <div class=\"container\">\n    <form class=\"login-form\" [formGroup]=\"editForm\" (ngSubmit)=\"onSubmit()\">\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <div class=\"form-group\">\n            <label for=\"newpname\" class=\"text-uppercase\">Product</label>\n            <input type=\"text\" class=\"form-control\" placeholder=\"Game Name\" formControlName=\"newpname\">\n          </div>\n        </div>\n        <div class=\"col-md-6\">\n          <div class=\"form-group\">\n            <label for=\"newpcost\" class=\"text-uppercase\">Product Cost</label>\n            <input type=\"number\" class=\"form-control\" placeholder=\"Product Cost\" formControlName=\"newpcost\">\n          </div>\n        </div>\n      </div>\n      <div class=\"row\">\n        <div class=\"col-md-6\">\n          <div class=\"form-group\">\n            <span class=\"ui-float-label\">\n              <p-editor formControlName=\"newdescription\" [style]=\"{'height':'120px'}\"></p-editor>\n            </span>\n          </div>\n        </div>\n        <div class=\"col-md-6\">\n          <div class=\"form-group\">\n            <label for=\"newstatus\" class=\"text-uppercase\">Status</label>\n            <select class=\"form-control\" id=\"newstatus\" formControlName=\"newstatus\">\n              <option value=\"out-of-stock\">Out of Stock</option>\n              <option value=\"in-stock\">In-Stock</option>\n            </select>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-md-4\">\n              <img [src]=\"localUrl\" *ngIf=\"localUrl\" alt=\"\" width=\"150px\" height=\"150px\">\n            </div>\n            <div class=\"col-md-8\">\n              <div class=\"form-group\">\n                <label for=\"imagePath\" class=\"text-uppercase\">Upload Image</label>\n                <br>\n                <input type=\"file\" id=\"imagePath\" (change)=\"onUpload($event)\" #fileInput>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"form-check\">\n        <button type=\"submit\" class=\"btn btn-login float-right\">Submit</button>\n      </div>\n    </form>\n  </div>"

/***/ }),

/***/ "./src/app/areas/admin/product-management/edit-product/edit-product.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/edit-product/edit-product.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-login {\n  background: #DE6262;\n  color: #fff;\n  margin-right: 15px;\n  font-weight: 600; }\n"

/***/ }),

/***/ "./src/app/areas/admin/product-management/edit-product/edit-product.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/edit-product/edit-product.component.ts ***!
  \***************************************************************************************/
/*! exports provided: EditProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditProductComponent", function() { return EditProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_productServices_edit_list_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/productServices/edit-list.service */ "./src/app/services/productServices/edit-list.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_productServices_get_product_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/productServices/get-product.service */ "./src/app/services/productServices/get-product.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditProductComponent = /** @class */ (function () {
    function EditProductComponent(editData, route, getProduct, formBuild, root) {
        this.editData = editData;
        this.route = route;
        this.getProduct = getProduct;
        this.formBuild = formBuild;
        this.root = root;
    }
    EditProductComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.editForm = this.formBuild.group({
            newpname: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            newimagePath: null,
            newpcost: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            newdescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            newstatus: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
        });
        this.route.params.subscribe(function (params) {
            console.log(params.id);
            _this.getProduct.listProductData(params.id)
                .subscribe(function (res) {
                _this.userData = res;
                console.log(_this.userData);
                _this.editForm.patchValue({
                    newpname: _this.userData.pname,
                    newpcost: _this.userData.pcost,
                    newdescription: _this.userData.description,
                    newstatus: _this.userData.status,
                });
                _this.localUrl = _this.userData.imagePath;
            });
        });
    };
    EditProductComponent.prototype.onUpload = function (event) {
        var _this = this;
        if (event.target.files.length > 0) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.localUrl = event.target.result;
                ;
            };
            reader.readAsDataURL(event.target.files[0]);
            var file = event.target.files[0];
            this.editForm.get('newimagePath').setValue(file);
            console.log(file);
        }
    };
    EditProductComponent.prototype.SavaEditProduct = function () {
        var input = new FormData();
        input.append('newpname', this.editForm.get('newpname').value);
        input.append('newimagePath', this.editForm.get('newimagePath').value);
        input.append('newpcost', this.editForm.get('newpcost').value);
        input.append('newdescription', this.editForm.get('newdescription').value);
        input.append('newstatus', this.editForm.get('newstatus').value);
        console.log(input);
        return input;
    };
    EditProductComponent.prototype.onSubmit = function () {
        var _this = this;
        var formModel = this.SavaEditProduct();
        this.route.params.subscribe(function (params) {
            console.log(formModel);
            _this.editData.editData(formModel, params['id'])
                .subscribe(function (res) {
                res = res.data;
            });
        });
        if (localStorage.token) {
            if (localStorage.role == "admin") {
                this.root.navigate(['adminDashboard/products/listp']);
            }
            else {
                this.root.navigate(['dashboard/products/listp']);
            }
        }
    };
    EditProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-product',
            template: __webpack_require__(/*! ./edit-product.component.html */ "./src/app/areas/admin/product-management/edit-product/edit-product.component.html"),
            styles: [__webpack_require__(/*! ./edit-product.component.scss */ "./src/app/areas/admin/product-management/edit-product/edit-product.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_productServices_edit_list_service__WEBPACK_IMPORTED_MODULE_2__["EditListService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _services_productServices_get_product_service__WEBPACK_IMPORTED_MODULE_4__["GetProductService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], EditProductComponent);
    return EditProductComponent;
}());



/***/ }),

/***/ "./src/app/areas/admin/product-management/product-listing/product-listing.component.html":
/*!***********************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/product-listing/product-listing.component.html ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-admin-header></app-admin-header>\n<br>\n<br>\n<div class=\"container\">\n  <h1>Detailed Data</h1>\n  <hr>\n</div>\n\n<div class=\"container\">\n  <div class=\"table-responsive\">\n    <p-table [value]=\"products\" [paginator]=\"true\" [rows]=\"5\">\n      <ng-template pTemplate=\"header\">\n        <tr>\n          <th>Product</th>\n          <th>Product Name</th>\n          <th>Product Cost</th>\n          <th>Description</th>\n          <th>Status</th>\n          <th>Operation</th>\n        </tr>\n      </ng-template>\n      <ng-template pTemplate=\"body\" let-product>\n        <tr>\n          <td style=\"padding-left: 5%\">\n            <img src=\"{{product.imagePath}}\" alt=\"\" width=\"100px\" height=\"100px\">\n          </td>\n          <td  style=\"word-wrap:break-word\">{{product.pname | uppercase}}</td>\n          <td>{{product.pcost}}</td>\n          <td>\n            <p-scrollPanel [style]=\"{width: '100%', height: '200px'}\">\n              <div style=\"word-wrap:break-word\" [innerHTML]=\"product.description\"></div>\n            </p-scrollPanel>\n          </td>\n          <td>{{product.status | uppercase}}</td>\n          <td>\n            <a class=\"btn btn-login\" [routerLink]=\"['/adminDashboard/products/editp',product._id]\">\n              <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>\n            </a>\n            <button type=\"button\" class=\"btn btn-login\" (click)=\"showDialog(product._id)\">\n              <i class=\"fa fa-trash\" aria-hidden=\"true\"></i>\n            </button>\n          </td>\n        </tr>\n      </ng-template>\n    </p-table>\n    <hr>\n  </div>\n</div>\n\n<p-dialog header=\"Delete\" [(visible)]=\"display\" [modal]=\"true\" [responsive]=\"true\" [width]=\"350\" [minWidth]=\"200\" [minY]=\"70\"\n  [maximizable]=\"false\" [baseZIndex]=\"10000\">\n  <span>Are you  sure !</span>\n  <p-footer>\n      <!-- <button button type=\"submit\" class=\"btn btn-login\" (click)=delete(product._id)>\n        <i class=\"fa fa-trash-o\" aria-hidden=\"true\"></i>\n      </button> -->\n    <button type=\"button\"  class=\"btn btn-login\"  (click)=\"display=false\">No</button>\n    <button type=\"button\"   class=\"btn btn-login\" (click)=delete(_id)>Yes</button>\n  </p-footer>\n</p-dialog>"

/***/ }),

/***/ "./src/app/areas/admin/product-management/product-listing/product-listing.component.scss":
/*!***********************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/product-listing/product-listing.component.scss ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-login {\n  background: #DE6262;\n  color: #fff;\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/areas/admin/product-management/product-listing/product-listing.component.ts":
/*!*********************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/product-listing/product-listing.component.ts ***!
  \*********************************************************************************************/
/*! exports provided: ProductListingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListingComponent", function() { return ProductListingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_productServices_list_data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../services/productServices/list-data.service */ "./src/app/services/productServices/list-data.service.ts");
/* harmony import */ var _services_productServices_delete_list_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/productServices/delete-list.service */ "./src/app/services/productServices/delete-list.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductListingComponent = /** @class */ (function () {
    function ProductListingComponent(listGame, deleteList) {
        this.listGame = listGame;
        this.deleteList = deleteList;
        this.display = false;
    }
    ProductListingComponent.prototype.ngOnInit = function () {
        this.getDetails();
    };
    ProductListingComponent.prototype.getDetails = function () {
        var _this = this;
        this.listGame.listData().subscribe(function (res) {
            _this.products = res;
        });
    };
    ProductListingComponent.prototype.showDialog = function (id) {
        this.display = true;
        this._id = id;
        console.log(this._id);
    };
    ProductListingComponent.prototype.delete = function (id) {
        this.display = false;
        console.log("id:", id);
        this.deleteList.deleteData(id).subscribe(function (res) {
            console.log("deleted");
        });
        this.getDetails();
    };
    ProductListingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product-listing',
            template: __webpack_require__(/*! ./product-listing.component.html */ "./src/app/areas/admin/product-management/product-listing/product-listing.component.html"),
            styles: [__webpack_require__(/*! ./product-listing.component.scss */ "./src/app/areas/admin/product-management/product-listing/product-listing.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_productServices_list_data_service__WEBPACK_IMPORTED_MODULE_1__["ListDataService"],
            _services_productServices_delete_list_service__WEBPACK_IMPORTED_MODULE_2__["DeleteListService"]])
    ], ProductListingComponent);
    return ProductListingComponent;
}());



/***/ }),

/***/ "./src/app/areas/admin/product-management/product-management-routing.module.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/product-management-routing.module.ts ***!
  \*************************************************************************************/
/*! exports provided: ProductManagementRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductManagementRoutingModule", function() { return ProductManagementRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_product_add_product_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./add-product/add-product.component */ "./src/app/areas/admin/product-management/add-product/add-product.component.ts");
/* harmony import */ var _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product-listing/product-listing.component */ "./src/app/areas/admin/product-management/product-listing/product-listing.component.ts");
/* harmony import */ var _edit_product_edit_product_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edit-product/edit-product.component */ "./src/app/areas/admin/product-management/edit-product/edit-product.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: "addp", component: _add_product_add_product_component__WEBPACK_IMPORTED_MODULE_3__["AddProductComponent"] },
    { path: "listp", component: _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_4__["ProductListingComponent"] },
    { path: "editp/:id", component: _edit_product_edit_product_component__WEBPACK_IMPORTED_MODULE_5__["EditProductComponent"] },
];
var ProductManagementRoutingModule = /** @class */ (function () {
    function ProductManagementRoutingModule() {
    }
    ProductManagementRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: []
        })
    ], ProductManagementRoutingModule);
    return ProductManagementRoutingModule;
}());



/***/ }),

/***/ "./src/app/areas/admin/product-management/product-management.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/areas/admin/product-management/product-management.module.ts ***!
  \*****************************************************************************/
/*! exports provided: ProductManagementModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductManagementModule", function() { return ProductManagementModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _add_product_add_product_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-product/add-product.component */ "./src/app/areas/admin/product-management/add-product/add-product.component.ts");
/* harmony import */ var _edit_product_edit_product_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edit-product/edit-product.component */ "./src/app/areas/admin/product-management/edit-product/edit-product.component.ts");
/* harmony import */ var _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product-listing/product-listing.component */ "./src/app/areas/admin/product-management/product-listing/product-listing.component.ts");
/* harmony import */ var _product_management_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! .//product-management-routing.module */ "./src/app/areas/admin/product-management/product-management-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _admin_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../admin.module */ "./src/app/areas/admin/admin.module.ts");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/table */ "./node_modules/primeng/table.js");
/* harmony import */ var primeng_table__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(primeng_table__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtext */ "./node_modules/primeng/inputtext.js");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/editor */ "./node_modules/primeng/editor.js");
/* harmony import */ var primeng_editor__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(primeng_editor__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/dialog.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(primeng_dialog__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/scrollpanel */ "./node_modules/primeng/scrollpanel.js");
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_13__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var ProductManagementModule = /** @class */ (function () {
    function ProductManagementModule() {
    }
    ProductManagementModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _product_management_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductManagementRoutingModule"],
                _admin_module__WEBPACK_IMPORTED_MODULE_7__["AdminModule"],
                primeng_table__WEBPACK_IMPORTED_MODULE_8__["TableModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_9__["DropdownModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__["InputTextModule"],
                primeng_editor__WEBPACK_IMPORTED_MODULE_11__["EditorModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_12__["DialogModule"], primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_13__["ScrollPanelModule"],
            ],
            declarations: [_add_product_add_product_component__WEBPACK_IMPORTED_MODULE_2__["AddProductComponent"],
                _edit_product_edit_product_component__WEBPACK_IMPORTED_MODULE_3__["EditProductComponent"],
                _product_listing_product_listing_component__WEBPACK_IMPORTED_MODULE_4__["ProductListingComponent"],
            ]
        })
    ], ProductManagementModule);
    return ProductManagementModule;
}());



/***/ }),

/***/ "./src/app/services/productServices/add-data.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/productServices/add-data.service.ts ***!
  \**************************************************************/
/*! exports provided: AddDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddDataService", function() { return AddDataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddDataService = /** @class */ (function () {
    function AddDataService(http) {
        this.http = http;
    }
    AddDataService.prototype.addData = function (data) {
        console.log("Added", data);
        return this.http.post('http://localhost:10010/products', data);
    };
    AddDataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AddDataService);
    return AddDataService;
}());



/***/ }),

/***/ "./src/app/services/productServices/edit-list.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/productServices/edit-list.service.ts ***!
  \***************************************************************/
/*! exports provided: EditListService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditListService", function() { return EditListService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EditListService = /** @class */ (function () {
    function EditListService(http) {
        this.http = http;
    }
    EditListService.prototype.editData = function (data, id) {
        console.log("edit", data);
        return this.http.put('http://localhost:10010/products?id=' + id, data);
    };
    EditListService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EditListService);
    return EditListService;
}());



/***/ }),

/***/ "./src/app/services/productServices/get-product.service.ts":
/*!*****************************************************************!*\
  !*** ./src/app/services/productServices/get-product.service.ts ***!
  \*****************************************************************/
/*! exports provided: GetProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetProductService", function() { return GetProductService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GetProductService = /** @class */ (function () {
    function GetProductService(http) {
        this.http = http;
    }
    GetProductService.prototype.listProductData = function (id) {
        return this.http.get('http://127.0.0.1:10010/getProduct?id=' + id);
    };
    GetProductService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], GetProductService);
    return GetProductService;
}());



/***/ })

}]);
//# sourceMappingURL=product-management-product-management-module.js.map