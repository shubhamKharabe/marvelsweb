(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["areas-users-users-module~blogs-blogs-module~products-products-module"],{

/***/ "./src/app/areas/users/dashboard/dashboard.component.html":
/*!****************************************************************!*\
  !*** ./src/app/areas/users/dashboard/dashboard.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-user-header></app-user-header>\n<img src=\".../../assets/images/11.jpg\" alt=\"\" width=\"100%\">\n<!-- <div class=\"container\" style=\"padding: 40px 20px\">\n    <div class=\"row\">\n        <div class=\"col-md-4 login-sec\">\n            <img class=\"d-block img-fluid\" src=\"../../assets/images/7.jpg\" alt=\"First slide\">\n            <h2>This is Heaven</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n        </div>\n        <div class=\"col-md-4 login-sec\">\n            <img class=\"d-block img-fluid\" src=\"../../assets/images/7.jpg\" alt=\"First slide\">\n            <h2>This is Heaven</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n        </div>\n        <div class=\"col-md-4 login-sec\">\n            <img class=\"d-block img-fluid\" src=\"../../assets/images/7.jpg\" alt=\"First slide\">\n            <h2>This is Heaven</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore\n                magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>\n        </div>\n    </div>\n</div> -->\n<app-user-footer></app-user-footer>"

/***/ }),

/***/ "./src/app/areas/users/dashboard/dashboard.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/areas/users/dashboard/dashboard.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-login {\n  background: #DE6262;\n  color: #fff;\n  font-weight: 600; }\n"

/***/ }),

/***/ "./src/app/areas/users/dashboard/dashboard.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/areas/users/dashboard/dashboard.component.ts ***!
  \**************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(root) {
        this.root = root;
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.logout = function () {
        localStorage.clear();
        this.root.navigate(['/']);
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/areas/users/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/areas/users/dashboard/dashboard.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/areas/users/edit-user-profile/edit-user-profile.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-user-header></app-user-header>\n\n<div class=\"container\">\n  <div class=\"container\">\n    <img src=\".../../assets/images/l1.png\" alt=\"First slide\" height=\"110px\" width=\"100%\">\n  </div>\n  <br>\n  <br>\n  <br>\n  <form class=\"form\" [formGroup]=\"editUserForm\" (ngSubmit)=\"onSubmit()\">\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <label for=\"newuname\" class=\"text-uppercase\">User Name</label>\n        <input type=\"text\" class=\"form-control\" placeholder=\"User Name\" formControlName=\"newuname\">\n      </div>\n\n      <div class=\"col-md-6\">\n        <label for=\"newgender\" class=\"text-uppercase\">Gender</label>\n        <select class=\"form-control\" id=\"newgender\" formControlName=\"newgender\">\n          <option value=\"male\">Male</option>\n          <option value=\"female\">Female</option>\n          <option value=\"other\">Other</option>\n        </select>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"col-md-6\">\n        <label for=\"newemail\" class=\"text-uppercase\">Email Address</label>\n        <input type=\"email\" class=\"form-control\" placeholder=\"Email Address\" formControlName=\"newemail\">\n      </div>\n\n      <div class=\"col-md-6\">\n      </div>\n    </div>\n    <div class=\"row\">\n    </div>\n    <br>\n    <div class=\"form-check\">\n      <button type=\"submit\" class=\"btn btn-login float-right\">Submit</button>\n    </div>\n  </form>\n  <br><br><br><br>\n  <br><br><br><br>\n  <br><br>\n\n</div>\n\n<app-user-footer></app-user-footer>"

/***/ }),

/***/ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/areas/users/edit-user-profile/edit-user-profile.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-login {\n  background: #4ec67f;\n  color: #fff;\n  margin-right: 5px; }\n"

/***/ }),

/***/ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/areas/users/edit-user-profile/edit-user-profile.component.ts ***!
  \******************************************************************************/
/*! exports provided: EditUserProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserProfileComponent", function() { return EditUserProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_userService_edit_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../services/userService/edit-user.service */ "./src/app/services/userService/edit-user.service.ts");
/* harmony import */ var _services_userService_edit_profile_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/userService/edit-profile-user.service */ "./src/app/services/userService/edit-profile-user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EditUserProfileComponent = /** @class */ (function () {
    function EditUserProfileComponent(editUser, root, editProfile) {
        this.editUser = editUser;
        this.root = root;
        this.editProfile = editProfile;
    }
    EditUserProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.editUserForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            "newuname": new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            "newpsw": new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            "newbdate": new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            "newemail": new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            "newgender": new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            "newrole": new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
        });
        this.data = { userId: localStorage.getItem("id") };
        // console.log("***********",this.data.token)
        this.editProfile.edit(this.data)
            .subscribe(function (res) {
            _this.userData = res;
            _this.id = res[0]._id;
            console.log("=====================", _this.userData);
            _this.editUserForm.patchValue({
                newuname: _this.userData[0].uname,
                newpsw: _this.userData[0].psw,
                newbdate: _this.userData[0].bdate,
                newemail: _this.userData[0].email,
                newgender: _this.userData[0].gender,
                newrole: _this.userData[0].role,
            });
        });
    };
    EditUserProfileComponent.prototype.onSubmit = function () {
        console.log(this.editUserForm.value);
        this.editUser.editUser(this.editUserForm.value, this.id)
            .subscribe(function (res) {
            res = res.data;
        });
        if (localStorage.token) {
            if (localStorage.role == "admin") {
                this.root.navigate(['adminDashboard/users/listu']);
            }
            else {
                this.root.navigate(['dashboard/']);
            }
        }
    };
    EditUserProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-edit-user-profile',
            template: __webpack_require__(/*! ./edit-user-profile.component.html */ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.html"),
            styles: [__webpack_require__(/*! ./edit-user-profile.component.scss */ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_userService_edit_user_service__WEBPACK_IMPORTED_MODULE_3__["EditUserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_userService_edit_profile_user_service__WEBPACK_IMPORTED_MODULE_4__["EditProfileUserService"]])
    ], EditUserProfileComponent);
    return EditUserProfileComponent;
}());



/***/ }),

/***/ "./src/app/areas/users/user-footer/user-footer.component.html":
/*!********************************************************************!*\
  !*** ./src/app/areas/users/user-footer/user-footer.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!-- Footer -->\n<section id=\"footer\">\n  <div class=\"container\">\n    <div class=\"row text-center text-xs-center text-sm-left text-md-left\">\n      <div class=\"col-md-3\">\n        <h5>Quick links</h5>\n        <ul class=\"list-unstyled quick-links\">\n          <li>\n            <a href=\"javascript:void();\">\n              <i class=\"fa fa-angle-double-right\"></i>Home</a>\n          </li>\n          <li>\n            <a href=\"javascript:void();\">\n              <i class=\"fa fa-angle-double-right\"></i>About</a>\n          </li>\n          <li>\n            <a href=\"javascript:void();\">\n              <i class=\"fa fa-angle-double-right\"></i>FAQ</a>\n          </li>\n          <li>\n            <a href=\"javascript:void();\">\n              <i class=\"fa fa-angle-double-right\"></i>Get Started</a>\n          </li>\n          <li>\n            <a href=\"javascript:void();\">\n              <i class=\"fa fa-angle-double-right\"></i>Videos</a>\n          </li>\n          <li>\n            <img src=\"/assets/images/groot.gif\" alt=\"Marvel\" id=\"icons\" width=\"100%\" height=\"120px\"> -Im Groot!\n          </li>\n        </ul>\n      </div>\n      <div class=\"col-md-9\">\n        <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d405817.7712316993!2d78.9205381850737!3d21.068715045999785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd4c772d0488d0b%3A0xe3a6693e3acc8f82!2ssmartData+Enterprises+(I)+Ltd.!5e1!3m2!1sen!2sin!4v1539081923628\"\n          width=\"100%\" height=\"300\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\n      </div>\n    </div>\n    <div class=\"section-title style2 text-center\">\n      <br>\n      <a href=\"#\">© the_Commmando 2018. All right reserved.</a>\n    </div>\n  </div>\n</section>\n<!-- ./Footer -->"

/***/ }),

/***/ "./src/app/areas/users/user-footer/user-footer.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/areas/users/user-footer/user-footer.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "section {\n  padding: 50px 0; }\n\nsection .section-title {\n  text-align: center;\n  color: #4ec67f;\n  text-transform: uppercase; }\n\n#footer {\n  background: #4ec67f !important; }\n\n#footer h5 {\n  padding-left: 10px;\n  border-left: 3px solid #eeeeee;\n  color: #ffffff; }\n\n#footer a {\n  color: #ffffff;\n  text-decoration: none !important;\n  background-color: transparent;\n  -webkit-text-decoration-skip: objects; }\n\n#footer ul.social li {\n  padding: 3px 0; }\n\n#footer ul.social li a i {\n  margin-right: 5px;\n  font-size: 25px;\n  transition: .5s all ease; }\n\n#footer ul.social li:hover a i {\n  font-size: 30px;\n  margin-top: -10px; }\n\n#footer ul.social li a,\n#footer ul.quick-links li a {\n  color: #ffffff; }\n\n#footer ul.social li a:hover {\n  color: #eeeeee; }\n\n#footer ul.quick-links li {\n  padding: 3px 0;\n  transition: .5s all ease; }\n\n#footer ul.quick-links li:hover {\n  padding: 3px 0;\n  margin-left: 5px;\n  font-weight: 700; }\n\n#footer ul.quick-links li a i {\n  margin-right: 5px; }\n\n#footer ul.quick-links li:hover a i {\n  font-weight: 700; }\n\n@media (max-width: 767px) {\n  #footer h5 {\n    padding-left: 0;\n    border-left: transparent;\n    padding-bottom: 0px;\n    margin-bottom: 10px; } }\n\n#icons:hover {\n  height: 120px;\n  /* Safari */\n  transition: width 2s; }\n"

/***/ }),

/***/ "./src/app/areas/users/user-footer/user-footer.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/areas/users/user-footer/user-footer.component.ts ***!
  \******************************************************************/
/*! exports provided: UserFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserFooterComponent", function() { return UserFooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var UserFooterComponent = /** @class */ (function () {
    function UserFooterComponent() {
    }
    UserFooterComponent.prototype.ngOnInit = function () {
    };
    UserFooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-footer',
            template: __webpack_require__(/*! ./user-footer.component.html */ "./src/app/areas/users/user-footer/user-footer.component.html"),
            styles: [__webpack_require__(/*! ./user-footer.component.scss */ "./src/app/areas/users/user-footer/user-footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], UserFooterComponent);
    return UserFooterComponent;
}());



/***/ }),

/***/ "./src/app/areas/users/user-header/user-header.component.html":
/*!********************************************************************!*\
  !*** ./src/app/areas/users/user-header/user-header.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<nav class=\"navbar sticky-top navbar-expand-sm  navbar-dark\">\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n  <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\n    <ul class=\"navbar-nav\">\n      <li class=\"nav-item\">\n        <a class=\"navbar-brand\" routerLink=\"/dashboard\">\n          <i class=\"fa fa-tachometer\" aria-hidden=\"true\"> Dashboard</i>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a id=\"button-basic\" routerLink=\"/dashboard/product/listp\">\n          <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"> Shop</i>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a id=\"button-basic\" routerLink=\"/dashboard/blog/listb\">\n          <i class=\"fa fa-inbox\" aria-hidden=\"true\"> Blogs</i>\n        </a>\n      </li>\n    </ul>\n  </div>\n  <div class=\"btn-group\" dropdown>\n    <a id=\"button-basic\" dropdownToggle type=\"\" class=\"btn dropdown-toggle\" aria-controls=\"dropdown-basic\">Hello {{uname}} !</a>\n    <ul id=\"dropdown-basic\" *dropdownMenu class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"button-basic\">\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item btn\" (click)=onClick()>\n          <i class=\"fa fa-user\" aria-hidden=\"true\"> Edit Profile</i></a>\n      </li>\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item btn\" routerLink=\"/dashboard/product/cart\">\n          <i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"> Cart</i></a>\n      </li>\n      <li role=\"menuitem\">\n        <a class=\"dropdown-item btn\" (click)=logout()>\n          <i class=\"fa fa-sign-out\" aria-hidden=\"true\"> Logout</i></a>\n      </li>\n    </ul>\n  </div>\n</nav>"

/***/ }),

/***/ "./src/app/areas/users/user-header/user-header.component.scss":
/*!********************************************************************!*\
  !*** ./src/app/areas/users/user-header/user-header.component.scss ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "a {\n  padding: 3px;\n  font-size: 17px;\n  margin-right: 25px; }\n\n#button-basic {\n  color: white;\n  font-size: 18px; }\n\n.navbar-brand {\n  font-size: 19px;\n  font-weight: bold; }\n\n.dropdown-item {\n  padding: 2px 0px 2px 20px; }\n\n.btn-login {\n  background: #4ec67f;\n  color: #fff; }\n\nnav {\n  background: #4ec67f;\n  padding: 13px; }\n"

/***/ }),

/***/ "./src/app/areas/users/user-header/user-header.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/areas/users/user-header/user-header.component.ts ***!
  \******************************************************************/
/*! exports provided: UserHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserHeaderComponent", function() { return UserHeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserHeaderComponent = /** @class */ (function () {
    function UserHeaderComponent(root) {
        this.root = root;
        this.uname = localStorage.uname;
    }
    UserHeaderComponent.prototype.ngOnInit = function () {
    };
    UserHeaderComponent.prototype.logout = function () {
        localStorage.clear();
        this.root.navigate(['/']);
    };
    UserHeaderComponent.prototype.onClick = function () {
        this.root.navigate(['/dashboard/users/editProfile']);
    };
    ;
    UserHeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-header',
            template: __webpack_require__(/*! ./user-header.component.html */ "./src/app/areas/users/user-header/user-header.component.html"),
            styles: [__webpack_require__(/*! ./user-header.component.scss */ "./src/app/areas/users/user-header/user-header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], UserHeaderComponent);
    return UserHeaderComponent;
}());



/***/ }),

/***/ "./src/app/areas/users/users-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/areas/users/users-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: UsersRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersRoutingModule", function() { return UsersRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/areas/users/dashboard/dashboard.component.ts");
/* harmony import */ var _edit_user_profile_edit_user_profile_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./edit-user-profile/edit-user-profile.component */ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: "", component: _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
    { path: "product", loadChildren: "./products/products.module#ProductsModule" },
    { path: "blog", loadChildren: "./blogs/blogs.module#BlogsModule" },
    { path: "users/editProfile", component: _edit_user_profile_edit_user_profile_component__WEBPACK_IMPORTED_MODULE_4__["EditUserProfileComponent"] },
];
var UsersRoutingModule = /** @class */ (function () {
    function UsersRoutingModule() {
    }
    UsersRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            declarations: []
        })
    ], UsersRoutingModule);
    return UsersRoutingModule;
}());



/***/ }),

/***/ "./src/app/areas/users/users.module.ts":
/*!*********************************************!*\
  !*** ./src/app/areas/users/users.module.ts ***!
  \*********************************************/
/*! exports provided: UsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _users_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./users-routing.module */ "./src/app/areas/users/users-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard/dashboard.component */ "./src/app/areas/users/dashboard/dashboard.component.ts");
/* harmony import */ var _user_header_user_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-header/user-header.component */ "./src/app/areas/users/user-header/user-header.component.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _edit_user_profile_edit_user_profile_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./edit-user-profile/edit-user-profile.component */ "./src/app/areas/users/edit-user-profile/edit-user-profile.component.ts");
/* harmony import */ var _user_footer_user_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user-footer/user-footer.component */ "./src/app/areas/users/user-footer/user-footer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var UsersModule = /** @class */ (function () {
    function UsersModule() {
    }
    UsersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _users_routing_module__WEBPACK_IMPORTED_MODULE_2__["UsersRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_6__["BsDropdownModule"].forRoot()
            ],
            declarations: [_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"],
                _user_header_user_header_component__WEBPACK_IMPORTED_MODULE_5__["UserHeaderComponent"],
                _edit_user_profile_edit_user_profile_component__WEBPACK_IMPORTED_MODULE_7__["EditUserProfileComponent"],
                _user_footer_user_footer_component__WEBPACK_IMPORTED_MODULE_8__["UserFooterComponent"],
                _user_footer_user_footer_component__WEBPACK_IMPORTED_MODULE_8__["UserFooterComponent"],
            ],
            exports: [
                _user_header_user_header_component__WEBPACK_IMPORTED_MODULE_5__["UserHeaderComponent"],
                _user_footer_user_footer_component__WEBPACK_IMPORTED_MODULE_8__["UserFooterComponent"],
            ]
        })
    ], UsersModule);
    return UsersModule;
}());



/***/ }),

/***/ "./src/app/services/userService/edit-user.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/userService/edit-user.service.ts ***!
  \***********************************************************/
/*! exports provided: EditUserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditUserService", function() { return EditUserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EditUserService = /** @class */ (function () {
    function EditUserService(http) {
        this.http = http;
    }
    EditUserService.prototype.editUser = function (data, id) {
        console.log("edit", data);
        return this.http.put('http://localhost:10010/users?id=' + id, data);
    };
    EditUserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], EditUserService);
    return EditUserService;
}());



/***/ })

}]);
//# sourceMappingURL=areas-users-users-module~blogs-blogs-module~products-products-module.js.map