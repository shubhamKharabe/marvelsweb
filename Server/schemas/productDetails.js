var mongoose = require("mongoose");

var userSchema = new mongoose.Schema({
    pname: String,
    imagePath: String,
    pcost: Number,
    description: String,
    status: String,
});
module.exports = mongoose.model("ProductDetails", userSchema);