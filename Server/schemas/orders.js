var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var  ordersSchema = new mongoose.Schema({
    date: {type: Date, default: Date.now()},
    userId:{type: Schema.Types.ObjectId,ref:"UserDetails"},
    cartData: Array,
    paymentMethod: String,
    address : String,
    total : Number,
});

var ordersSchema = mongoose.model("orders", ordersSchema);
module.exports =ordersSchema;







