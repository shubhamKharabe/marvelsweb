var mongoose = require("mongoose");
var blogSchema = new mongoose.Schema({
    title: String,
    img: String,
    description: String,
    date: Date,
    rating: Number,
    comments: Array,
});
module.exports = mongoose.model("BlogDetails", blogSchema);
