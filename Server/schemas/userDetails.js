var mongoose = require("mongoose")

var userSchema = new mongoose.Schema({
    token: String,
    uname: { type: String, unique: true },
    psw: String,
    bdate: Date,
    email: { type: String, unique: true },
    gender: String,
    role: String,
    resetPasswordToken: String,
    resetPasswordExpires: Date,
});
module.exports = mongoose.model("UserDetails", userSchema);




