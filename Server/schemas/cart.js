var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var  cartSchema = new mongoose.Schema({
    date: {type: Date, default: Date.now()},
    userId:{type: Schema.Types.ObjectId,ref:"UserDetails"},
    productId:{type: Schema.Types.ObjectId,ref:"ProductDetails"},
    productCost: Number,
    quantity : Number,
    total : Number,
});

var cartSchema = mongoose.model("cart", cartSchema);
module.exports =cartSchema;


























// var cart = {
//     userId:mongoose.Types.ObjectId("5bbd93a1364efc3a1fd28165"),
//     productId:mongoose.Types.ObjectId("5bbc61cefa35944fc60d1808"),
//     qty : 5,
//     total :1000
// };

// conn.collection('cartSchema').insert(cart);
