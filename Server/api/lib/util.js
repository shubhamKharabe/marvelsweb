'use strict';


var jwt = require('jsonwebtoken');
const User = require('../../schemas/userDetails');
module.exports = {
    ensureAuthorized: ensureAuthorized
}

function ensureAuthorized(req, res, next) {
    var unauthorizedJson = { code: 401, 'message': 'No Activity, Please Login Again!', data: [] };
    // var token = req.headers["authorization"];
    var tokenWIthBearer = req.headers.authorization.split(' ')
    console.log("******************************", tokenWIthBearer);

    if (typeof tokenWIthBearer !== 'undefined') {
        // var splitToken = token.split(' ');
        // console.log("123123121212", splitToken);
        try {

            var token = tokenWIthBearer[1];
            let decoded = jwt.verify(token, 'shubham');
            console.log("Decoded Data--------------", decoded);
            if (tokenWIthBearer[0] == 'Bearer') {
                User.findOne({ _id: decoded._id }).exec(function (err, user) {
                    if (err || !user) {
                        res.json(unauthorizedJson);
                    } else {
                        req.user = user;
                        next();
                    }
                });
            }
        } catch (err) {
            console.log("gAHAPA", err);
            res.json(unauthorizedJson);
        }
    } else {
        res.json(unauthorizedJson);
    }
}