var orders = require("../../schemas/orders");
var cart = require("../../schemas/cart");

module.exports = {
    addToOrders: addToOrders,
    listOrders: listOrders,
};
function addToOrders(req, res) {
    var orderRecord = new orders();

    orderRecord.date = Date.now();
    orderRecord.userId = req.body.userId;
    orderRecord.address = req.body.address;
    orderRecord.total = req.body.total;
    cart.find({ userId: orderRecord.userId }).sort({ date: 'descending' }).populate({
        path: 'productId',
        model: 'ProductDetails',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 400,
                message: "please try again"
            })
        } else {
            orderRecord.cartData = data;
            console.log("*****************************", orderRecord.cartData);
            orderRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'OrderDetails not Added',
                        data: []
                    })
                } else {
                    res.json({
                        code: 200,
                        message: 'Order Accepted',
                        data: response
                    })
                }
            })
        }
    })
}
function listOrders(req, res) {

    orders.find({ userId: req.swagger.params.userId.value }).sort({ date: 'descending' }).populate({
        path: 'cartData.productId',
        model: 'ProductDetails',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 400,
                message: "please try again"
            })
        } else {
            res.json(data);
        }
    })
}

