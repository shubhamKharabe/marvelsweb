var Products = require("../../schemas/productDetails");
var fs = require('fs');
module.exports = {
    addProduct: addProduct,
    listProduct: listProduct,
    deleteProduct: deleteProduct,
    editProduct: editProduct,
    getProduct: getProduct,
};

//***********************************Adduser***********************************
function addProduct(req, res) {
    //Uplaod Image
    timeStamp = Date.now();
    orignalImageName = timeStamp + '_' + req.files.imagePath[0].originalname;
    console.log(req.files.imagePath[0])
    var imagePath = '../Server/public/uploads/' + orignalImageName;
    fs.writeFile(imagePath, (req.files.imagePath[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })
    // Uplaod Image Ends
    let serverIp = "http://localhost:10010/uploads/";
    var product = new Products();
    product.pname = req.swagger.params.pname.value;
    product.pcost = req.swagger.params.pcost.value;
    product.description = req.swagger.params.description.value;
    product.status = req.swagger.params.status.value;
    product.imagePath = serverIp + orignalImageName;

    product.save(product, function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'Not Added',
                data: []
            })
        } else {
            res.json({
                code: 200,
                data: response,
                message: 'Successfully Product Added'
            })
        }
    })
}
//***********************************List Users.*********************************** 
function listProduct(req, res) {
    Products.find(function (err, data) {
        console.log("List", data)
        res.json(data);
    });
}
//***********************************Delete Users.*********************************** 
function deleteProduct(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    Products.findByIdAndRemove(_id, function (err, data) {
        if (err) {
            res.json({
                code: 404,
                message: 'Not Deteted',
                data: []
            })
        } else {
            res.json({
                code: 200,
                data: data,
                message: 'Product Data Deleted'
            })
        }
    });
};
//***********************************Get Product*********************************
function getProduct(req, res) {
    let _id = req.swagger.params.id.value
    Products.findById(_id, function (err, data) {
        console.log("product", data)
        res.json(data);
    });
}
//*********************************Edit Products.********************************
function editProduct(req, res) {
    console.log(req.files)
    if (!req.files.newimagePath) {
        let _id = req.swagger.params.id.value
        console.log("**********************************", _id);
        var newp = req.swagger.params.newpname.value;
        var newpc = req.swagger.params.newpcost.value;
        var newsc = req.swagger.params.newdescription.value;
        var news = req.swagger.params.newstatus.value;
        Products.findByIdAndUpdate(_id, {
            $set: {
                pname: newp,
                pcost: newpc,
                description: newsc,
                status: news,
            }
        }, function (err, data) {
            if (err) {
                res.json({
                    code: 404,
                    message: 'Not Updated',
                    data: []
                })
            } else {
                data.save();
                res.json({
                    code: 200,
                    data: data,
                    message: 'Product Data Updated'
                })
            }
        });

    } else {
        // Uplaod Image
        timeStamp = Date.now();
        orignalImageName = timeStamp + '_' + req.files.newimagePath[0].originalname;
        console.log(req.files.newimagePath[0])
        var imagePath = '../Server/public/uploads/' + orignalImageName;
        fs.writeFile(imagePath, (req.files.newimagePath[0].buffer), function (err) {
            if (err) throw err;
            console.log("Product Image Uploaded");
        })
        // Uplaod Iamge Ends
        let serverIp = "http://localhost:10010/uploads/";
        let _id = req.swagger.params.id.value
        console.log("**********************************", _id);
        var newp = req.swagger.params.newpname.value;
        var newpc = req.swagger.params.newpcost.value;
        var newsc = req.swagger.params.newdescription.value;
        var news = req.swagger.params.newstatus.value;
        var newi = serverIp + orignalImageName;
        Products.findByIdAndUpdate(_id, {
            $set: {
                pname: newp,
                pcost: newpc,
                description: newsc,
                status: news,
                imagePath: newi
            }
        }, function (err, data) {
            if (err) {
                res.json({
                    code: 404,
                    message: 'Not Updated',
                    data: []
                })
            } else {
                data.save();
                res.json({
                    code: 200,
                    data: data,
                    message: 'Product Data Updated'
                })
            }
        });

    }
};
