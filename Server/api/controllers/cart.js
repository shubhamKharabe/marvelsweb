
//---------********************** Cart Module ***************-------------------------


var cart = require("../../schemas/cart");
module.exports = {
    addToCart: addToCart,
    listCart: listCart,
    editCart: editCart,
    deleteCart: deleteCart,
    deleteAllCart: deleteAllCart,
};
function addToCart(req, res) {

    var cartRecord = new cart();
    cartRecord.date = Date.now();
    cartRecord.userId = req.body.userId;
    cartRecord.productId = req.body.productId;
    cartRecord.productCost = req.body.productCost;
    cartRecord.quantity = (req.body.quantity);
    cartRecord.total = (cartRecord.quantity * cartRecord.productCost);
    console.log(cartRecord);

    cart.findOne({ "userId": cartRecord.userId, 'productId': cartRecord.productId }, function (err, data) {
        if (err) throw err;
        else if (data) {
            console.log("same product");
            console.log("Complete Product Data", data);
            data.quantity = Number(cartRecord.quantity + data.quantity);
            data.total = (cartRecord.productCost * data.quantity);
            cart.updateOne({ "productId": data.productId }, { $set: data }, function (err, result) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'Not Added',
                        data: []
                    })
                } else {
                    res.json({
                        code: 200,
                        data: result,
                        message: 'Product Added To Cart!'
                    })
                }
            });
        }
        else {
            cartRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'Not Added',
                        data: []
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response,
                        message: 'Product Added To Cart!'
                    })
                }
            })
        }
    })
}



function listCart(req, res) {
    cart.find({ userId: req.swagger.params.userId.value }).sort({ date: 'descending' }).populate({
        path: 'productId',
        model: 'ProductDetails',
    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if (err) {
            res.json({
                code: 400,
                message: "please try again"
            })
        } else {
            res.json(data);
        }
    })

}

function editCart(req, res) {

    var cartRecord = new cart();
    cartRecord.date = Date.now();
    cartRecord.userId = req.body.userId;
    cartRecord.productId = req.body.productId;
    cartRecord.productCost = req.body.productCost;
    cartRecord.quantity = (req.body.quantity);
    cartRecord.total = (cartRecord.quantity * cartRecord.productCost);
    console.log(cartRecord);

    cart.findOne({ "userId": cartRecord.userId, 'productId': cartRecord.productId }, function (err, data) {
        if (err) throw err;
        else if (data) {
            console.log("same product");
            console.log("Complete Product Data", data);
            data.quantity = Number(cartRecord.quantity);
            data.total = (cartRecord.productCost * data.quantity);
            cart.updateOne({ "productId": data.productId }, { $set: data }, function (err, result) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'Not Updated',
                        data: []
                    })
                } else {
                    res.json({
                        code: 200,
                        data: result,
                        message: 'Cart Updated Successfully!'
                    })
                }
            });
        }
        else {
            cartRecord.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'Cart-Details Not Updated',
                        data: []
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response,
                        message:  'Cart Updated Successfully!'
                    })
                }
            })
        }
    })
}


function deleteCart(req, res) {
    let id = req.swagger.params.id.value;
    console.log("Id to delete", id);
    cart.findByIdAndRemove({ _id: id }, function (err, data) {
        if (err) {
            res.json({
                code: 404,
                message: 'Product Not Removed From Cart',
                data: []
            })
        } else {
            data.save();
            res.json({
                code: 200,
                data: data,
                message:  'Product Removed From Cart!'
            })
        }
    });
}



function deleteAllCart(req, res) {
    let id = req.swagger.params.id.value;
    console.log("Id to delete", id);
    cart.remove({ userId: id }, function (err, data) {
        if (err) throw err;
        console.log("Product Deleted Sucessfully from Cart!", data);
        // data.save();
        res.json(data);
    });

}