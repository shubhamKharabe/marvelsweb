var user = require("../../schemas/userDetails");
// var cart = require("../../schemas/cart");
const jwt = require('jsonwebtoken');
var async = require("async");
var crypto = require("crypto");
var nodemailer = require('nodemailer');

module.exports = {
    login: login,
    forgotPassword: forgotPassword,
    resetPassword: resetPassword,
    addUser: addUser,
    listUser: listUser,
    deleteUser: deleteUser,
    editUser: editUser,
    getUser: getUser,
    editProfile: editProfile,
};


//*************************Login****************************
function login(req, res) {
    let data = {
        uname: req.body.uname,
        psw: req.body.psw
    };
    // var record = new user();
    user.findOne(data, (err, data) => {
        if (err) {
            return res.json({ message: 'User not found!', code: 404, data: [] });
        } else if (!data) {
            return res.json({ message: 'Wrong Credentials!', code: 404, data: [] });
        } else {
            console.log(data);
            console.log("Done!");
            let token = jwt.sign(data.toJSON(), 'shubham', {
                expiresIn: '14000000000000' // expires in 1 hour
            });
            console.log(token)
            res.json({ token: token, user: data, code: 200, message: 'Login Successfull!' });
        }
    });
}

//*************************Forgot Password****************************
function forgotPassword(req, res, next) {
    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            user.findOne({ email: req.body.email }, function (err, user) {
                if (!user) {
                    return res.json({
                        message: 'No account with that email address exists.',
                        code: 404,
                    });
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function (err) {
                    done(err, token, user);
                });
            });
        },
        function (token, user, done) {
            var transOptions = {
                host: 'smtp.gmail.com',
                port: 465,
                secure: true, // use SSL
                auth: {
                    user: 'marvels.of.india.007@gmail.com',
                    pass: '992140360073',
                }
            };
            var smtpTransport = nodemailer.createTransport(transOptions);
            var mailOptions = {
                to: user.email,
                from: 'marvels.of.india.007@gmail.com',
                subject: 'Node.js Password Reset',
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    // 'http://' + req.headers.host + '/resetPassword/' + token + '\n\n' +
                    'http://localhost:4200/login/resetPassword/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                done(err, 'done');
            });
        }
    ], function (err) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            console.log("Mail Sent Successfully!");
            res.json({
                code: 200,
                message: "Mail Sent Successfully!"
            });
        }
    });
};

function resetPassword(req, res) {
    async.waterfall([
        function (done) {
            console.log(req.swagger.params.token.value);
            user.findOne({ resetPasswordToken: req.swagger.params.token.value, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
                if (!user) {
                    return res.json({
                        message: 'Password reset token is invalid or has expired.',
                        code: 404,
                    });
                }

                user.psw = req.body.psw;
                user.resetPasswordToken = undefined;
                user.resetPasswordExpires = undefined;

                user.save(function (err) {
                    done(err, user);
                });
            });
        },
        function (user, done) {
            var transOptions = {
                host: 'smtp.gmail.com',
                port: 465,
                secure: true, // use SSL
                auth: {
                    user: 'marvels.of.india.007@gmail.com',
                    pass: '992140360073',
                }
            };
            var smtpTransport = nodemailer.createTransport(transOptions);
            var mailOptions = {
                to: user.email,
                from: 'marvels.of.india.007@gmail.com',
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                    'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                done(err, 'done');
            });
        }
    ], function (err) {
        if (err) {
            console.log(err);
            return next(err);
        }
        else {
            res.json({
                code: 200,
                message: "Reset Password Successfully!"
            });
        }
    });
};
// ****************************Adduser****************************
function addUser(req, res) {
    var record = new user();
    record.uname = req.body.uname;
    record.psw = req.body.psw;
    record.email = req.body.email;
    record.gender = req.body.gender;
    record.bdate = req.body.bdate;
    record.role = req.body.role || 'user';

    record.save(record, function (err, response) {
        if (err) {
            res.json({
                message: 'Not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response,
                message: 'User Added Successfully'
            })
        }
    })
}

//****************************List All User****************************
function listUser(req, res) {
    user.find({ role: 'user' }, function (err, data) {
        res.json(data);
    });
}

//****************************Delete Users****************************
function deleteUser(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    user.findByIdAndRemove(_id, function (err, data) {
        console.log(data);
        if (err) {
            res.json({
                code: 404,
                message: 'Not Added',
                data: []
            })
        } else {
            res.json({
                code: 200,
                data: data,
                message: 'User Deleted'
            });
        }
    });
};

//****************************Edit Users****************************
function editUser(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    var newu = req.body.newuname;
    var newp = req.body.newpsw;
    var newe = req.body.newemail;
    var newg = req.body.newgender;
    var newb = req.body.newbdate;
    var newr = req.body.newrole;
    user.findByIdAndUpdate(_id, { $set: { uname: newu, psw: newp, email: newe, gender: newg, bdate: newb, role: newr } }, function (err, data) {
        if (err) {
            res.json({
                code: 404,
                data: [],
                message: 'User Data Not Updated'
            });
        } else {
            data.save();
            res.json({
                code: 200,
                data: data,
                message: 'User Data Updated'
            });
        }
    });
};
//****************************Get a prticular User****************************
function getUser(req, res) {
    let _id = req.swagger.params.id.value
    user.findById(_id, function (err, data) {
        console.log("user", data)
        res.json(data);
    });
}
//****************************Edit Profile****************************
function editProfile(req, res) {
    let id = req.body.userId;
    // console.log("SAD", tok)
    user.find({ _id: id }, function (err, data) {
        console.log("user", data)
        res.json(data);
    });
}

















































    // try {
    //     let user = mongoose.Types.ObjectId(req.body.user);
    //     console.log("userId:**************", id)
        // user.aggregate([
        //     { $match: { "_id": id } },
        //     { $unwind: "$cart" },
        //     {
        //         $lookup: {
        //             "from": "productdetails",
        //             "localField": "cart.productId",
        //             "foreignField": "_id",
        //             "as": "productInfo"
        //         }
        //     },
        //     { $unwind: "$productInfo" },
        //     {
        //         $group: {
        //             "_id": "$_id",
        //             "cart": { "$push": "$cart" },
        //             "productInfo": { "$push": "$productInfo" }
        //         }
        //     }
        // ], function (err, data) {
        //     if (err) {
        //         console.log("errr", err)
        //     } else {
        //         // console.log("data", data)
        //     }
            // res.json(id);
        // });;
    // } catch (error) {
    //     return res.json({
    //         code: 404,
    //         message: 'Error'
    //     })
    // }