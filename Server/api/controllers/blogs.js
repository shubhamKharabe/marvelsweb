var Blogs = require("../../schemas/blogDetails");

var fs = require('fs');
module.exports = {
    addBlog: addBlog,
    listBlog: listBlog,
    deleteBlog: deleteBlog,
    getBlog: getBlog,
    editBlog: editBlog,
    getBlogComments: getBlogComments,
    addComment: addComment,
};

//***********************addBlog************************
function addBlog(req, res) {
    //Uplaod Image
    timeStamp = Date.now();
    orignalImageName = timeStamp + '_' + req.files.img[0].originalname;
    console.log(req.files.img[0])
    var img = '../Server/public/uploads/blogs/' + orignalImageName;
    fs.writeFile(img, (req.files.img[0].buffer), function(err) {
        if (err) throw err;
        console.log("Blog Image Uploaded");
    })
    // Uplaod Iamge Ends
    var blog = new Blogs();
    let serverIp = "http://localhost:10010/uploads/blogs/";
    blog.title = req.swagger.params.title.value;
    blog.description = req.swagger.params.description.value;
    blog.date = req.swagger.params.date.value;
    blog.img = serverIp + orignalImageName;

    blog.save(blog, function(err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'Not Added',
                data: []
            })
        } else {
            res.json({
                code: 200,
                data: response,
                message: 'Blog Added Successfully'
            })
        }
    })
}

//*******************List Blogs*************************** 
function listBlog(req, res) {
    // Blogs.find(function (err, data) {
    //     console.log("List", data)
    //     res.json(data);
    // });
    var queryArray = [{
        $project: {
            title: '$title',
            description: '$description',
            date: '$date',
            img: '$img',
            rating: { $avg: "$comments.rating" },
            comments: '$comments'
        }
    }];
    Blogs.aggregate(queryArray, function(err, data) {
        res.json(data);
    });
}

//*********************Delete Blogs****************************
function deleteBlog(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    Blogs.findByIdAndRemove(_id, function(err, data) {
        if (err) {
            res.json({
                code: 404,
                message: 'Blog Not Removed',
                data: []
            })
        } else {
            res.json({
                code: 200,
                data: data,
                message: 'Blog Deleted Successfully!'
            })
        }
    });
};

//*************************Get Blog Comments***************************
function getBlogComments(req, res) {
    let _id = req.swagger.params.id.value
    Blogs.findById(_id, function(err, data) {
        console.log("Blog Comments", data)
        res.json(data);
    });
}

//***************************Get Blog********************************

function getBlog(req, res) {
    let _id = req.swagger.params.id.value
    Blogs.findById(_id, function(err, data) {
        console.log("Blog:", data)
        res.json(data);
    });
}
//***************************Edit Blogs********************************
function editBlog(req, res) {

    if (!req.files.img) {
        let _id = req.swagger.params.id.value
        console.log(_id);
        var newtitle = req.swagger.params.newtitle.value;
        var newdescription = req.swagger.params.newdescription.value;
        Blogs.findByIdAndUpdate(_id, {
            $set: {
                title: newtitle,
                description: newdescription,
            }
        }, function(err, data) {
            if (err) {
                res.json({
                    code: 404,
                    message: 'Blog Not Updated',
                    data: []
                })
            } else {
                data.save();
                res.json({
                    code: 200,
                    data: data,
                    message: 'Blog Updated Successfully!'
                })
            }
        });
    } else {
        //Uplaod Image
        timeStamp = Date.now();
        orignalImageName = timeStamp + '_' + req.files.newimg[0].originalname;
        console.log(req.files.newimg[0])
        var img = '../Server/public/uploads/blogs/' + orignalImageName;
        fs.writeFile(img, (req.files.newimg[0].buffer), function(err) {
            if (err) throw err;
            console.log("Product Image Uploaded");
        })
        //Uplaod Iamge Ends

        let _id = req.swagger.params.id.value
        console.log(_id);
        var newtitle = req.swagger.params.newtitle.value;
        var newdescription = req.swagger.params.newdescription.value;
        var newi = "http://localhost:10010/uploads/blogs/" + orignalImageName;
        Blogs.findByIdAndUpdate(_id, {
            $set: {
                title: newtitle,
                description: newdescription,
                img: newi
            }
        }, function(err, data) {
            if (err) {
                res.json({
                    code: 404,
                    message: 'Blog Not Updated',
                    data: []
                })
            } else {
                data.save();
                res.json({
                    code: 200,
                    data: data,
                    message: 'Blog Updated Successfully!'
                })
            }
        });

    }
};

//*************************Add Comment*****************************
function addComment(req, res) {
    console.log(req.body)
    try {

        if (!req.body.blogId) {
            return null
        }
        let blogId = req.body.blogId;
        let comment = [{
            userId: req.body.userId,
            comment: req.body.comment,
            rating: req.body.rating
        }];
        console.log("asdasdsa", comment);
        Blogs.findByIdAndUpdate(blogId, {
            $push: {
                comments: comment
            }
        }, function(err, data) {
            if (err) {
                res.json({
                    code: 404,
                    message: 'Comment Not Updated',
                    data: []
                })
            } else {
                data.save();
                res.json({
                    code: 200,
                    data: data,
                    message: 'Comment Added Successfully!'
                })
            }
        });
    } catch (error) {
        return res.json({
            code: 404,
            message: 'Error'
        })
    }
};