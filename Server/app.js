'use strict';

var SwaggerExpress = require('swagger-express-mw');
var express = require('express');
var app = express()//require('express')();
var path = require('path');
var cors = require('cors')
var utils = require('./api/lib/util')
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};
console.log(config.appRoot)

var mongoose = require("mongoose", { useNewUrlParser: true });
// mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/Mean1");
mongoose.set('debug', true);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Database connection successful!');
});
app.use(express.static('public'))
app.use(cors())
app.use('/api/*', function (req, res, next) {
  console.log(req.baseUrl)
  var freeAuthPath = [
    '/api/login', 
    '/api/forgotPassword',
    '/api/Payment',
  ];
  var available = false;
  for (var i = 0; i < freeAuthPath.length; i++) {
    if (freeAuthPath[i] == req.baseUrl) {
      available = true;
      break;
    }
  }
  if (!available) {
    utils.ensureAuthorized(req, res, next);
  } else {
    next();
  }
});

SwaggerExpress.create(config, function (err, swaggerExpress) {
  if (err) { throw err; }
  //Swagger Ui
  app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  // install middleware
  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/users']) {
    console.log('try this:\ncurl http://127.0.0.1:' + port + '/docs');
  }
});
